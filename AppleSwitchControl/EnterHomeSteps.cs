﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleSwitchControl
{
    public abstract class EnterHomeSteps
    {
        //"FirstPage, RestoreCompletedPage, BuddyWiFiController, BuddyMesaEnrollmentController,BuddyPasscodeController,BuddyiOSTCController, BuddyLocationServicesController,FaceIdModelOtherControllers,BuddyFinishedController",
        public const string FIRST_PAGE = "FirstPage";
        public const string RESTORE_COMPLETED_PAGE = "RestoreCompletedPage";
        public const string BUDDY_WIFI_CONTROLLER = "BuddyWiFiController";
        public const string BUDDY_MESA_ENROLLMENT_CONTROLLER = "BuddyMesaEnrollmentController";
        public const string BUDDY_PASSCODE_CONTROLLER = "BuddyPasscodeController";
        public const string BUDDY_IOS_TC_CONTROLLER = "BuddyiOSTCController";
        public const string BUDDY_LOCATION_SERVICES_CONTROLLER = "BuddyLocationServicesController";
        public const string BUDDY_FINISHED_CONTROLLER = "BuddyFinishedController";
        public const string ENTER_HOME_PAGE_DONE = "EnterHomePageDone";

        public const string OTHER_CONTROLLERS = "OtherControllers";


        public const int PAGE_CHANGE_WAIT_READY = 2500;
        public const int SAME_PAGE_STEP_INTERVAL = 300;


        public abstract string HandleFirstPageController();
        public abstract string HandleRestoreCompletedPage(bool supervisorMode);
        public abstract string HandleBuddyWiFiController(bool supervisor);
        public abstract string HandleBuddyMesaEnrollmentController();
        public abstract string HandleBuddyPasscodeController();
        public abstract string HandleBuddyiOSTCController();
        public abstract string HandleBuddyLocationServicesController();
        public abstract string HandleBuddyFinishedController();

        public virtual string HandleOtherControllers()
        {
            return null;
        }
    }
}
