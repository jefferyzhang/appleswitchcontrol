﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleSwitchControl
{
    class FaceIdPhoneEnterHomeSteps : PhoneEnterHomeSteps
    {
        private int nDelay = 100;
        public FaceIdPhoneEnterHomeSteps(IOSKeyInput iOSKeyInput, Random random, int delay) : base(iOSKeyInput, random)
        {
            nDelay = delay;
        }

        public override string HandleFirstPageController()
        {
            mIOSKeyInput.sendKey(0x4a);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return RESTORE_COMPLETED_PAGE;
        }

        public override string HandleBuddyMesaEnrollmentController()
        {
            // this is FaceID page
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return BUDDY_PASSCODE_CONTROLLER;
        }

        public override string HandleBuddyLocationServicesController()
        {
            base.HandleBuddyLocationServicesController();
            return OTHER_CONTROLLERS;
        }

        public override string HandleBuddyFinishedController()
        {
            // must send double "home" key quickly
            mIOSKeyInput.sendKey(0x48);
            System.Threading.Thread.Sleep(2000);

            mIOSKeyInput.sendKey(0x4a);
            System.Threading.Thread.Sleep(nDelay);
            mIOSKeyInput.sendKey(0x4a);

            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return ENTER_HOME_PAGE_DONE;
        }

        public override string HandleOtherControllers()
        {
            // manual page: "True Tone", "Go Home", "Recent Apps" and "Control Center"
            trueTonePage();
            goHomePage();
            recentAppsPage();
            controlCenterPage();
            return BUDDY_FINISHED_CONTROLLER;
        }

        private void trueTonePage()
        {
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }

        private void goHomePage()
        {
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }

        private void recentAppsPage()
        {
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }

        private void controlCenterPage()
        {
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }
    }
}
