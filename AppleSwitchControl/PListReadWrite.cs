﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace iDeviceRestoreBackup
{
    public enum plistType
    {
        Auto,
        Binary,
        Xml
    }

    public static class PlistDateConverter
    {
        public static long timeDifference = 978307200L;

        public static long GetAppleTime(long unixTime)
        {
            return unixTime - PlistDateConverter.timeDifference;
        }

        public static long GetUnixTime(long appleTime)
        {
            return appleTime + PlistDateConverter.timeDifference;
        }

        public static System.DateTime ConvertFromAppleTimeStamp(double timestamp)
        {
            System.DateTime dateTime = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            return dateTime.AddSeconds(timestamp);
        }

        public static double ConvertToAppleTimeStamp(System.DateTime date)
        {
            System.DateTime d = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            return System.Math.Floor((date - d).TotalSeconds);
        }
    }
    public static class PListReadWrite
    {
        private static List<int> offsetTable = new List<int>();

        private static List<byte> objectTable = new List<byte>();

        private static int refCount;

        private static int objRefSize;

        private static int offsetByteSize;

        private static long offsetTableOffset;

        public static bool InsertLineBreaks = false;

        public static bool IsFilterSpecialCharacter = true;

        public static string CorrectURL(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return "";
            }
            string text = Regex.Replace(fileName, "[\"\\0�]\u007f", "");
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                char[] invalidPathChars = Path.GetInvalidPathChars();
                for (int i = 0; i < text.Length; i++)
                {
                    char c = text[i];
                    int num = (int)c;
                    if (num != 65533 && num != 127 && num != 124 && num != 5)
                    {
                        bool flag = true;
                        char[] array = invalidPathChars;
                        for (int j = 0; j < array.Length; j++)
                        {
                            if (array[j] == c)
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                        {
                            stringBuilder.Append(text[i]);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //LogMessager.WriteInfo(string.Format("StackTrace:{0}---Message:{1}", ex.StackTrace, ex.Message));
            }
            return stringBuilder.ToString();
        }


        public static object readPlist(string path)
        {
            object result;
            using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                result = PListReadWrite.readPlist(fileStream, plistType.Auto);
            }
            return result;
        }

        public static object readPlistSource(string source)
        {
            return PListReadWrite.readPlist(Encoding.UTF8.GetBytes(source));
        }

        public static object readPlist(byte[] data)
        {
            if (data != null)
            {
                return PListReadWrite.readPlist(new MemoryStream(data), plistType.Auto);
            }
            return null;
        }

        public static plistType getPlistType(Stream stream)
        {
            byte[] array = new byte[8];
            stream.Read(array, 0, 8);
            if (BitConverter.ToInt64(array, 0) == 3472403351741427810L)
            {
                return plistType.Binary;
            }
            return plistType.Xml;
        }

        public static object readPlist(Stream stream, plistType type = plistType.Auto)
        {
            if (type == plistType.Auto)
            {
                type = PListReadWrite.getPlistType(stream);
                stream.Seek(0L, SeekOrigin.Begin);
            }
            if (type == plistType.Binary)
            {
                using (BinaryReader binaryReader = new BinaryReader(stream))
                {
                    return PListReadWrite.readBinary(binaryReader.ReadBytes((int)binaryReader.BaseStream.Length));
                }
            }
            XmlDocument expr_49 = new XmlDocument();
            expr_49.XmlResolver = null;
            expr_49.Load(stream);
            return PListReadWrite.readXml(expr_49);
        }

        public static void writeXml(object value, string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(path, false))
            {
                streamWriter.Write(PListReadWrite.writeXml(value));
            }
        }

        public static void writeXml(object value, Stream stream)
        {
            using (StreamWriter streamWriter = new StreamWriter(stream))
            {
                streamWriter.Write(PListReadWrite.writeXml(value));
            }
        }

        public static string writeXml(object value)
        {
            string @string;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream, new XmlWriterSettings
                {
                    Encoding = new UTF8Encoding(false),
                    ConformanceLevel = ConformanceLevel.Document,
                    Indent = true
                }))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteDocType("plist", "-//Apple Computer//DTD PLIST 1.0//EN", "http://www.apple.com/DTDs/PropertyList-1.0.dtd", null);
                    xmlWriter.WriteStartElement("plist");
                    xmlWriter.WriteAttributeString("version", "1.0");
                    PListReadWrite.compose(value, xmlWriter);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Flush();
                    xmlWriter.Close();
                    @string = Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
            return @string;
        }

        public static void writeBinary(object value, string path)
        {
            using (BinaryWriter binaryWriter = new BinaryWriter(new FileStream(path, FileMode.OpenOrCreate)))
            {
                binaryWriter.Write(PListReadWrite.writeBinary(value));
            }
        }

        public static void writeBinary(object value, Stream stream)
        {
            using (BinaryWriter binaryWriter = new BinaryWriter(stream))
            {
                binaryWriter.Write(PListReadWrite.writeBinary(value));
            }
        }

        public static byte[] writeBinary(object value)
        {
            PListReadWrite.offsetTable.Clear();
            PListReadWrite.objectTable.Clear();
            PListReadWrite.refCount = 0;
            PListReadWrite.objRefSize = 0;
            PListReadWrite.offsetByteSize = 0;
            PListReadWrite.offsetTableOffset = 0L;
            int num = PListReadWrite.countObject(value) - 1;
            PListReadWrite.refCount = num;
            PListReadWrite.objRefSize = PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(PListReadWrite.refCount)).Length;
            PListReadWrite.composeBinary(value);
            PListReadWrite.writeBinaryString("bplist00", false);
            PListReadWrite.offsetTableOffset = (long)PListReadWrite.objectTable.Count;
            PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count - 8);
            PListReadWrite.offsetByteSize = PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(PListReadWrite.offsetTable[PListReadWrite.offsetTable.Count - 1])).Length;
            List<byte> list = new List<byte>();
            PListReadWrite.offsetTable.Reverse();
            for (int i = 0; i < PListReadWrite.offsetTable.Count; i++)
            {
                PListReadWrite.offsetTable[i] = PListReadWrite.objectTable.Count - PListReadWrite.offsetTable[i];
                byte[] array = PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(PListReadWrite.offsetTable[i]), PListReadWrite.offsetByteSize);
                Array.Reverse(array);
                list.AddRange(array);
            }
            PListReadWrite.objectTable.AddRange(list);
            PListReadWrite.objectTable.AddRange(new byte[6]);
            PListReadWrite.objectTable.Add(Convert.ToByte(PListReadWrite.offsetByteSize));
            PListReadWrite.objectTable.Add(Convert.ToByte(PListReadWrite.objRefSize));
            byte[] bytes = BitConverter.GetBytes((long)num + 1L);
            Array.Reverse(bytes);
            PListReadWrite.objectTable.AddRange(bytes);
            PListReadWrite.objectTable.AddRange(BitConverter.GetBytes(0L));
            bytes = BitConverter.GetBytes(PListReadWrite.offsetTableOffset);
            Array.Reverse(bytes);
            PListReadWrite.objectTable.AddRange(bytes);
            return PListReadWrite.objectTable.ToArray();
        }

        private static object readXml(XmlDocument xml)
        {
            Object obj = PListReadWrite.parse(xml.DocumentElement.ChildNodes[0]);
            //return (Dictionary<string, object>)PListReadWrite.parse(xml.DocumentElement.ChildNodes[0]);
            return obj;
        }

        private static object readBinary(byte[] data)
        {
            PListReadWrite.offsetTable.Clear();
            List<byte> offsetTableBytes = new List<byte>();
            PListReadWrite.objectTable.Clear();
            PListReadWrite.refCount = 0;
            PListReadWrite.objRefSize = 0;
            PListReadWrite.offsetByteSize = 0;
            PListReadWrite.offsetTableOffset = 0L;
            List<byte> list = new List<byte>(data);
            PListReadWrite.parseTrailer(list.GetRange(list.Count - 32, 32));
            if (PListReadWrite.offsetTableOffset < (long)list.Count)
            {
                PListReadWrite.objectTable = list.GetRange(0, (int)PListReadWrite.offsetTableOffset);
                offsetTableBytes = list.GetRange((int)PListReadWrite.offsetTableOffset, list.Count - (int)PListReadWrite.offsetTableOffset - 32);
            }
            PListReadWrite.parseOffsetTable(offsetTableBytes);
            return PListReadWrite.parseBinary(0);
        }

        private static Dictionary<string, object> parseDictionary(XmlNode node)
        {
            XmlNodeList childNodes = node.ChildNodes;
            if (childNodes.Count % 2 != 0)
            {
                throw new DataMisalignedException("Dictionary elements must have an even number of child nodes");
            }
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            for (int i = 0; i < childNodes.Count; i += 2)
            {
                XmlNode xmlNode = childNodes[i];
                XmlNode arg_54_0 = childNodes[i + 1];
                if (xmlNode.Name != "key")
                {
                    throw new ApplicationException("expected a key node");
                }
                object obj = PListReadWrite.parse(arg_54_0);
                if (obj != null && !dictionary.ContainsKey(xmlNode.InnerText))
                {
                    dictionary.Add(xmlNode.InnerText, obj);
                }
            }
            return dictionary;
        }

        private static List<object> parseArray(XmlNode node)
        {
            System.Collections.Generic.List<object> list = new System.Collections.Generic.List<object>();
            foreach (XmlNode node2 in node.ChildNodes)
            {
                object obj = PListReadWrite.parse(node2);
                if (obj != null)
                {
                    list.Add(obj);
                }
            }
            return list;
        }

        private static void composeArray(List<object> value, XmlWriter writer)
        {
            writer.WriteStartElement("array");
            using (List<object>.Enumerator enumerator = value.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    PListReadWrite.compose(enumerator.Current, writer);
                }
            }
            writer.WriteEndElement();
        }

        private static object parse(XmlNode node)
        {
            string name;
            switch (name = node.Name)
            {
                case "dict":
                    return PListReadWrite.parseDictionary(node);
                case "array":
                    return PListReadWrite.parseArray(node);
                case "string":
                    return node.InnerText;
                case "integer":
                    {
                        long num2 = 0L;
                        try
                        {
                            num2 = System.Convert.ToInt64(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                        }
                        catch (System.OverflowException)
                        {
                            ulong value = System.Convert.ToUInt64(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                            byte[] bytes = System.BitConverter.GetBytes(value);
                            num2 = System.BitConverter.ToInt64(bytes, 0);
                        }
                        return num2;
                    }
                case "real":
                    return System.Convert.ToDouble(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                case "false":
                    return false;
                case "true":
                    return true;
                case "null":
                    return null;
                case "date":
                    return XmlConvert.ToDateTime(node.InnerText, XmlDateTimeSerializationMode.Utc);
                case "data":
                    return System.Convert.FromBase64String(node.InnerText);
            }
            throw new System.ApplicationException(string.Format("Plist Node `{0}' is not supported", node.Name));
        }

        private static void compose(object value, XmlWriter writer)
        {
            if (value == null || value is string)
            {
                string text = value as string;
                if (PListReadWrite.IsFilterSpecialCharacter)
                {
                    text = CorrectURL(text);
                    writer.WriteElementString("string", text);
                    return;
                }
                writer.WriteElementString("string", text);
                return;
            }
            else
            {
                if (value is int)
                {
                    writer.WriteElementString("integer", ((int)value).ToString(NumberFormatInfo.InvariantInfo));
                    return;
                }
                if (value is uint)
                {
                    writer.WriteElementString("integer", ((uint)value).ToString(NumberFormatInfo.InvariantInfo));
                    return;
                }
                if (value is ushort)
                {
                    writer.WriteElementString("integer", ((ushort)value).ToString(NumberFormatInfo.InvariantInfo));
                    return;
                }
                if (value is long)
                {
                    writer.WriteElementString("integer", ((long)value).ToString(NumberFormatInfo.InvariantInfo));
                    return;
                }
                if (value is ulong)
                {
                    writer.WriteElementString("integer", ((ulong)value).ToString(NumberFormatInfo.InvariantInfo));
                    return;
                }
                if (value is Dictionary<string, object> || value.GetType().ToString().StartsWith("System.Collections.Generic.Dictionary`2[System.String"))
                {
                    Dictionary<string, object> dictionary = value as Dictionary<string, object>;
                    if (dictionary == null)
                    {
                        dictionary = new Dictionary<string, object>();
                        IDictionary dictionary2 = (IDictionary)value;
                        foreach (object current in dictionary2.Keys)
                        {
                            dictionary.Add(current.ToString(), dictionary2[current]);
                        }
                    }
                    PListReadWrite.writeDictionaryValues(dictionary, writer);
                    return;
                }
                if (value is List<object>)
                {
                    PListReadWrite.composeArray((List<object>)value, writer);
                    return;
                }
                if (value is byte[])
                {
                    if (!PListReadWrite.InsertLineBreaks)
                    {
                        writer.WriteElementString("data", Convert.ToBase64String((byte[])value));
                        return;
                    }
                    string value2 = Convert.ToBase64String((byte[])value, Base64FormattingOptions.InsertLineBreaks);
                    writer.WriteElementString("data", value2);
                    return;
                }
                else
                {
                    if (value is float || value is double)
                    {
                        writer.WriteElementString("real", ((double)value).ToString(NumberFormatInfo.InvariantInfo));
                        return;
                    }
                    if (value is DateTime)
                    {
                        DateTime value3 = (DateTime)value;
                        string text2 = "";
                        if (!PListReadWrite.InsertLineBreaks)
                        {
                            text2 = XmlConvert.ToString(value3, XmlDateTimeSerializationMode.Utc);
                        }
                        else
                        {
                            text2 = XmlConvert.ToString(value3, XmlDateTimeSerializationMode.Utc);
                            if (text2.Contains("."))
                            {
                                int num = text2.LastIndexOf(".");
                                try
                                {
                                    text2 = text2.Remove(num, text2.Length - num) + "Z";
                                }
                                catch
                                {
                                }
                            }
                        }
                        writer.WriteElementString("date", text2);
                        return;
                    }
                    if (value is bool)
                    {
                        writer.WriteElementString(value.ToString().ToLower(), "");
                        return;
                    }
                    throw new Exception(string.Format("Value type '{0}' is unhandled", value.GetType().ToString()));
                }
            }
        }

        private static void writeDictionaryValues(Dictionary<string, object> dictionary, XmlWriter writer)
        {
            try
            {
                writer.WriteStartElement("dict");
                foreach (string current in dictionary.Keys)
                {
                    object arg_35_0 = dictionary[current];
                    writer.WriteElementString("key", current);
                    PListReadWrite.compose(arg_35_0, writer);
                }
                writer.WriteEndElement();
            }
            catch (Exception )
            {
                //LogMessager.WriteInfo("Message:" + ex.ToString());
            }
        }

        private static int countObject(object value)
        {
            if (value == null)
            {
                return 0;
            }
            int num = 0;
            string a = value.GetType().ToString();
            if (!(a == "System.Collections.Generic.Dictionary`2[System.String,System.Object]"))
            {
                if (!(a == "System.Collections.Generic.List`1[System.Object]"))
                {
                    num++;
                }
                else
                {
                    foreach (object current in ((List<object>)value))
                    {
                        num += PListReadWrite.countObject(current);
                    }
                    num++;
                }
            }
            else
            {
                Dictionary<string, object> dictionary = (Dictionary<string, object>)value;
                foreach (string current2 in dictionary.Keys)
                {
                    num += PListReadWrite.countObject(dictionary[current2]);
                }
                num += dictionary.Keys.Count;
                num++;
            }
            return num;
        }

        private static byte[] writeBinaryDictionary(Dictionary<string, object> dictionary)
        {
            List<byte> list = new List<byte>();
            List<byte> list2 = new List<byte>();
            List<int> list3 = new List<int>();
            for (int i = dictionary.Count - 1; i >= 0; i--)
            {
                object[] array = new object[dictionary.Count];
                dictionary.Values.CopyTo(array, 0);
                PListReadWrite.composeBinary(array[i]);
                PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count);
                list3.Add(PListReadWrite.refCount);
                PListReadWrite.refCount--;
            }
            for (int j = dictionary.Count - 1; j >= 0; j--)
            {
                string[] array2 = new string[dictionary.Count];
                dictionary.Keys.CopyTo(array2, 0);
                PListReadWrite.composeBinary(array2[j]);
                PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count);
                list3.Add(PListReadWrite.refCount);
                PListReadWrite.refCount--;
            }
            if (dictionary.Count < 15)
            {
                list2.Add(Convert.ToByte((int)(208 | Convert.ToByte(dictionary.Count))));
            }
            else
            {
                list2.Add(223);
                list2.AddRange(PListReadWrite.writeBinaryInteger(dictionary.Count, false));
            }
            using (List<int>.Enumerator enumerator = list3.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    byte[] array3 = PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(enumerator.Current), PListReadWrite.objRefSize);
                    Array.Reverse(array3);
                    list.InsertRange(0, array3);
                }
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] composeBinaryArray(List<object> objects)
        {
            List<byte> list = new List<byte>();
            List<byte> list2 = new List<byte>();
            List<int> list3 = new List<int>();
            for (int i = objects.Count - 1; i >= 0; i--)
            {
                PListReadWrite.composeBinary(objects[i]);
                PListReadWrite.offsetTable.Add(PListReadWrite.objectTable.Count);
                list3.Add(PListReadWrite.refCount);
                PListReadWrite.refCount--;
            }
            if (objects.Count < 15)
            {
                list2.Add(Convert.ToByte((int)(160 | Convert.ToByte(objects.Count))));
            }
            else
            {
                list2.Add(175);
                list2.AddRange(PListReadWrite.writeBinaryInteger(objects.Count, false));
            }
            using (List<int>.Enumerator enumerator = list3.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    byte[] array = PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(enumerator.Current), PListReadWrite.objRefSize);
                    Array.Reverse(array);
                    list.InsertRange(0, array);
                }
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] composeBinary(object obj)
        {
            string key;
            switch (key = obj.GetType().ToString())
            {
                case "System.Collections.Generic.Dictionary`2[System.String,System.Object]":
                    return PListReadWrite.writeBinaryDictionary((System.Collections.Generic.Dictionary<string, object>)obj);
                case "System.Collections.Generic.List`1[System.Object]":
                    return PListReadWrite.composeBinaryArray((System.Collections.Generic.List<object>)obj);
                case "System.Byte[]":
                    return PListReadWrite.writeBinaryByteArray((byte[])obj);
                case "System.Double":
                    return PListReadWrite.writeBinaryDouble((double)obj);
                case "System.Int32":
                case "System.Int64":
                case "System.UInt32":
                    return PListReadWrite.writeBinaryInteger(obj, true);
                case "System.String":
                    {
                        int byteCount = System.Text.Encoding.UTF8.GetByteCount((string)obj);
                        int length = ((string)obj).Length;
                        byte[] result;
                        if (length == byteCount)
                        {
                            result = PListReadWrite.writeBinaryString((string)obj, true);
                        }
                        else
                        {
                            result = PListReadWrite.writeBinaryUniString((string)obj, true);
                        }
                        return result;
                    }
                case "System.DateTime":
                    return PListReadWrite.writeBinaryDate((System.DateTime)obj);
                case "System.Boolean":
                    return PListReadWrite.writeBinaryBool((bool)obj);
            }
            return new byte[0];
        }


        public static byte[] writeBinaryDate(DateTime obj)
        {
            List<byte> list = new List<byte>(PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(PlistDateConverter.ConvertToAppleTimeStamp(obj)), 8));
            list.Reverse();
            list.Insert(0, 51);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        public static byte[] writeBinaryBool(bool obj)
        {
            System.Collections.Generic.List<byte> list = new System.Collections.Generic.List<byte>(new byte[]
			{
				obj ? (byte)(9) : (byte)(8)
			});
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }


        private static byte[] writeBinaryInteger(object value, bool write)
        {
            List<byte> list;
            if (value.GetType().ToString().Equals("System.Int32"))
            {
                list = new List<byte>(BitConverter.GetBytes((long)((int)value)));
            }
            else if (value.GetType().ToString().Equals("System.UInt32"))
            {
                list = new List<byte>(BitConverter.GetBytes((long)((ulong)((uint)value))));
            }
            else
            {
                list = new List<byte>(BitConverter.GetBytes((long)value));
            }
            list = new List<byte>(PListReadWrite.RegulateNullBytes(list.ToArray()));
            while ((double)list.Count != Math.Pow(2.0, Math.Log((double)list.Count) / Math.Log(2.0)))
            {
                list.Add(0);
            }
            int value2 = 16 | (int)(Math.Log((double)list.Count) / Math.Log(2.0));
            list.Reverse();
            list.Insert(0, Convert.ToByte(value2));
            if (write)
            {
                PListReadWrite.objectTable.InsertRange(0, list);
            }
            return list.ToArray();
        }

        private static byte[] writeBinaryDouble(double value)
        {
            List<byte> list = new List<byte>(PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(value), 4));
            while ((double)list.Count != Math.Pow(2.0, Math.Log((double)list.Count) / Math.Log(2.0)))
            {
                list.Add(0);
            }
            int value2 = 32 | (int)(Math.Log((double)list.Count) / Math.Log(2.0));
            list.Reverse();
            list.Insert(0, Convert.ToByte(value2));
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryByteArray(byte[] value)
        {
            List<byte> list = new List<byte>(value);
            List<byte> list2 = new List<byte>();
            if (value.Length < 15)
            {
                list2.Add(Convert.ToByte((int)(64 | Convert.ToByte(value.Length))));
            }
            else
            {
                list2.Add(79);
                list2.AddRange(PListReadWrite.writeBinaryInteger(list.Count, false));
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryString(string value, bool head)
        {
            List<byte> list = new List<byte>();
            List<byte> list2 = new List<byte>();
            char[] array = value.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                char value2 = array[i];
                list.Add(Convert.ToByte(value2));
            }
            if (head)
            {
                if (value.Length < 15)
                {
                    list2.Add(Convert.ToByte((int)(80 | Convert.ToByte(value.Length))));
                }
                else
                {
                    list2.Add(95);
                    list2.AddRange(PListReadWrite.writeBinaryInteger(list.Count, false));
                }
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] writeBinaryUniString(string value, bool head)
        {
            List<byte> list = new List<byte>();
            List<byte> list2 = new List<byte>();
            UnicodeEncoding expr_11 = new UnicodeEncoding();
            byte[] array = new byte[expr_11.GetByteCount(value)];
            expr_11.GetBytes(value, 0, value.Length, array, 0);
            int num = 0;
            while (num < array.Length && num + 1 < array.Length)
            {
                byte b = array[num];
                array[num] = array[num + 1];
                array[num + 1] = b;
                num += 2;
            }
            byte[] array2 = array;
            for (int i = 0; i < array2.Length; i++)
            {
                byte item = array2[i];
                list.Add(item);
            }
            if (head)
            {
                if (value.Length < 15)
                {
                    list2.Add(Convert.ToByte((int)(96 | Convert.ToByte(value.Length))));
                }
                else
                {
                    list2.Add(111);
                    list2.AddRange(PListReadWrite.writeBinaryInteger(value.Length, false));
                }
            }
            list.InsertRange(0, list2);
            PListReadWrite.objectTable.InsertRange(0, list);
            return list.ToArray();
        }

        private static byte[] RegulateNullBytes(byte[] value)
        {
            return PListReadWrite.RegulateNullBytes(value, 1);
        }

        private static byte[] RegulateNullBytes(byte[] value, int minBytes)
        {
            Array.Reverse(value);
            List<byte> list = new List<byte>(value);
            int num = 0;
            while (num < list.Count && list[num] == 0 && list.Count > minBytes)
            {
                list.Remove(list[num]);
                num--;
                num++;
            }
            if (list.Count < minBytes)
            {
                int num2 = minBytes - list.Count;
                for (int i = 0; i < num2; i++)
                {
                    list.Insert(0, 0);
                }
            }
            value = list.ToArray();
            Array.Reverse(value);
            return value;
        }

        private static void parseTrailer(List<byte> trailer)
        {
            PListReadWrite.offsetByteSize = BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(trailer.GetRange(6, 1).ToArray(), 4), 0);
            PListReadWrite.objRefSize = BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(trailer.GetRange(7, 1).ToArray(), 4), 0);
            byte[] expr_4A = trailer.GetRange(12, 4).ToArray();
            Array.Reverse(expr_4A);
            PListReadWrite.refCount = BitConverter.ToInt32(expr_4A, 0);
            byte[] expr_69 = trailer.GetRange(24, 8).ToArray();
            Array.Reverse(expr_69);
            PListReadWrite.offsetTableOffset = BitConverter.ToInt64(expr_69, 0);
        }

        private static void parseOffsetTable(List<byte> offsetTableBytes)
        {
            if (PListReadWrite.offsetByteSize == 0)
            {
                PListReadWrite.offsetByteSize = 1;
            }
            for (int i = 0; i < offsetTableBytes.Count; i += PListReadWrite.offsetByteSize)
            {
                byte[] array = offsetTableBytes.GetRange(i, PListReadWrite.offsetByteSize).ToArray();
                Array.Reverse(array);
                PListReadWrite.offsetTable.Add(BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0));
            }
        }

        private static object parseBinaryDictionary(int objRef)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            List<int> list = new List<int>();
            byte arg_23_0 = PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]];
            int num;
            int count = PListReadWrite.getCount(PListReadWrite.offsetTable[objRef], out num);
            if (count < 15)
            {
                num = PListReadWrite.offsetTable[objRef] + 1;
            }
            else
            {
                num = PListReadWrite.offsetTable[objRef] + 2 + PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(count), 1).Length;
            }
            for (int i = num; i < num + count * 2 * PListReadWrite.objRefSize; i += PListReadWrite.objRefSize)
            {
                byte[] array = PListReadWrite.objectTable.GetRange(i, PListReadWrite.objRefSize).ToArray();
                Array.Reverse(array);
                list.Add(BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0));
            }
            for (int j = 0; j < count; j++)
            {
                dictionary.Add((string)PListReadWrite.parseBinary(list[j]), PListReadWrite.parseBinary(list[j + count]));
            }
            return dictionary;
        }

        private static object parseBinaryArray(int objRef)
        {
            List<object> list = new List<object>();
            List<int> list2 = new List<int>();
            byte arg_23_0 = PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]];
            int num;
            int count = PListReadWrite.getCount(PListReadWrite.offsetTable[objRef], out num);
            if (count < 15)
            {
                num = PListReadWrite.offsetTable[objRef] + 1;
            }
            else
            {
                num = PListReadWrite.offsetTable[objRef] + 2 + PListReadWrite.RegulateNullBytes(BitConverter.GetBytes(count), 1).Length;
            }
            for (int i = num; i < num + count * PListReadWrite.objRefSize; i += PListReadWrite.objRefSize)
            {
                byte[] array = PListReadWrite.objectTable.GetRange(i, PListReadWrite.objRefSize).ToArray();
                Array.Reverse(array);
                list2.Add(BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(array, 4), 0));
            }
            for (int j = 0; j < count; j++)
            {
                list.Add(PListReadWrite.parseBinary(list2[j]));
            }
            return list;
        }

        private static int getCount(int bytePosition, out int newBytePosition)
        {
            byte b = Convert.ToByte((int)(PListReadWrite.objectTable[bytePosition] & 15));
            int result;
            if (b < 15)
            {
                result = (int)b;
                newBytePosition = bytePosition + 1;
            }
            else
            {
                result = (int)PListReadWrite.parseBinaryInt(bytePosition + 1, out newBytePosition);
            }
            return result;
        }

        private static object parseBinary(int objRef)
        {
            byte b = 0;
            try
            {
                b = PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]];
            }
            catch
            {
                b = 0;
            }
            int num = (int)(b & 240);
            if (num <= 48)
            {
                if (num <= 16)
                {
                    if (num != 0)
                    {
                        if (num == 16)
                        {
                            return PListReadWrite.parseBinaryInt64(PListReadWrite.offsetTable[objRef]);
                        }
                    }
                    else
                    {
                        if (PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]] != 0)
                        {
                            return PListReadWrite.objectTable[PListReadWrite.offsetTable[objRef]] == 9;
                        }
                        return null;
                    }
                }
                else
                {
                    if (num == 32)
                    {
                        return PListReadWrite.parseBinaryReal(PListReadWrite.offsetTable[objRef]);
                    }
                    if (num == 48)
                    {
                        return PListReadWrite.parseBinaryDate(PListReadWrite.offsetTable[objRef]);
                    }
                }
            }
            else if (num <= 80)
            {
                if (num == 64)
                {
                    return PListReadWrite.parseBinaryByteArray(PListReadWrite.offsetTable[objRef]);
                }
                if (num == 80)
                {
                    return PListReadWrite.parseBinaryAsciiString(PListReadWrite.offsetTable[objRef]);
                }
            }
            else
            {
                if (num == 96)
                {
                    return PListReadWrite.parseBinaryUnicodeString(PListReadWrite.offsetTable[objRef]);
                }
                if (num == 160)
                {
                    return PListReadWrite.parseBinaryArray(objRef);
                }
                if (num == 208)
                {
                    return PListReadWrite.parseBinaryDictionary(objRef);
                }
            }
            return PListReadWrite.parseBinaryDictionary(objRef);
        }

        public static object parseBinaryDate(int headerPosition)
        {
            byte[] expr_13 = PListReadWrite.objectTable.GetRange(headerPosition + 1, 8).ToArray();
            Array.Reverse(expr_13);
            return PlistDateConverter.ConvertFromAppleTimeStamp(BitConverter.ToDouble(expr_13, 0));
        }

        private static object parseBinaryInt64(int headerPosition)
        {
            int num;
            return PListReadWrite.parseBinaryInt64(headerPosition, out num);
        }

        private static object parseBinaryInt(int headerPosition, out int newHeaderPosition)
        {
            byte b = PListReadWrite.objectTable[headerPosition];
            int num = (int)Math.Pow(2.0, (double)(b & 15));
            byte[] expr_34 = PListReadWrite.objectTable.GetRange(headerPosition + 1, num).ToArray();
            Array.Reverse(expr_34);
            newHeaderPosition = headerPosition + num + 1;
            return BitConverter.ToInt32(PListReadWrite.RegulateNullBytes(expr_34, 4), 0);
        }

        private static object parseBinaryInt64(int headerPosition, out int newHeaderPosition)
        {
            byte b = PListReadWrite.objectTable[headerPosition];
            int num = (int)Math.Pow(2.0, (double)(b & 15));
            byte[] expr_34 = PListReadWrite.objectTable.GetRange(headerPosition + 1, num).ToArray();
            Array.Reverse(expr_34);
            newHeaderPosition = headerPosition + num + 1;
            return BitConverter.ToInt64(PListReadWrite.RegulateNullBytes(expr_34, 8), 0);
        }

        private static object parseBinaryReal(int headerPosition)
        {
            byte b = PListReadWrite.objectTable[headerPosition];
            int count = (int)Math.Pow(2.0, (double)(b & 15));
            byte[] expr_34 = PListReadWrite.objectTable.GetRange(headerPosition + 1, count).ToArray();
            Array.Reverse(expr_34);
            return BitConverter.ToDouble(PListReadWrite.RegulateNullBytes(expr_34, 8), 0);
        }

        private static object parseBinaryAsciiString(int headerPosition)
        {
            int index;
            int count = PListReadWrite.getCount(headerPosition, out index);
            List<byte> range = PListReadWrite.objectTable.GetRange(index, count);
            if (range.Count <= 0)
            {
                return string.Empty;
            }
            return Encoding.ASCII.GetString(range.ToArray());
        }

        private static object parseBinaryUnicodeString(int headerPosition)
        {
            int num2;
            int num = PListReadWrite.getCount(headerPosition, out num2);
            num *= 2;
            byte[] array = new byte[num];
            for (int i = 0; i < num; i += 2)
            {
                byte b = PListReadWrite.objectTable.GetRange(num2 + i, 1)[0];
                byte b2 = PListReadWrite.objectTable.GetRange(num2 + i + 1, 1)[0];
                if (BitConverter.IsLittleEndian)
                {
                    array[i] = b2;
                    array[i + 1] = b;
                }
                else
                {
                    array[i] = b;
                    array[i + 1] = b2;
                }
            }
            return Encoding.Unicode.GetString(array);
        }

        private static object parseBinaryByteArray(int headerPosition)
        {
            int index;
            int count = PListReadWrite.getCount(headerPosition, out index);
            return PListReadWrite.objectTable.GetRange(index, count).ToArray();
        }
    }
}
