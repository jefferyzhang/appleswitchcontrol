﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AppleSwitchControl
{
    partial class Program
    {
        private const int PHONE_MODEL = 0;
        private const int FACE_ID_PHONE_MODEL = 1;
        private const int PAD_MODEL = 2;
        static bool faceIdModel = false;


        static Boolean bHasAutoJoinWifi = true;
        static int enter_homepage_v2(System.Collections.Specialized.StringDictionary args,
            List<object> PageNames)
        {
            int ret = 1;
            logIt($"enter_homepage_v2: ++ ");

            // check face ID model
            int nDelay = 100;
            if (args.ContainsKey("Model") && AppleSwitchControl.Properties.Settings.Default.faceModel.Contains(args["Model"]))
            {
                faceIdModel = true;
                String sjsonDelay = AppleSwitchControl.Properties.Settings.Default.DbClickInterval;
                var serializer = new JavaScriptSerializer();
                var jsondelay= (Dictionary<string, Object>)serializer.DeserializeObject(sjsonDelay);
                if (jsondelay.ContainsKey(args["Model"]))
                { nDelay = Convert.ToInt32(jsondelay[args["Model"]]); }
            }
            bool done = false;
            DateTime _last_page_click = DateTime.MinValue;
            DateTime _last_log_recved = DateTime.MinValue;
            DateTime _start = DateTime.Now;
            string page_name = "FirstPage";
            int index = 0;
            if (PageNames != null && PageNames.Count > 0) page_name = PageNames[index].ToString();
            while (!done && (DateTime.Now - _start).TotalSeconds < 300)
            {
                System.Threading.Thread.Sleep(1000);
                if ((DateTime.Now - _last_page_click).TotalSeconds > 2)
                {
                    logIt($"{page_name} handler triggered by timer.");

                    Tuple<DateTime, string> res = handle_EachPage(page_name, args, faceIdModel, nDelay);
                    _last_page_click = res.Item1;
                    page_name = res.Item2;
                    index++;
                    if (PageNames != null && PageNames.Count > index) page_name = PageNames[index].ToString();

                    if (string.Compare(page_name, "EnterHomePageDone", true) == 0)
                    {
                        // turn serial port off
                        //disconnectKeyInputComPort();

                        done = true;
                        ret = 0;
                    }
                    else if (string.Compare(page_name, "EnterHomePageFailed", true) == 0)
                    {
                        // turn serial port off
                        //disconnectKeyInputComPort();

                        done = true;
                        ret = 1;
                    }
                }
            }
            
            logIt($"enter_homepage_v2: -- ret={ret}");
            return ret;
        }

        static Tuple<DateTime, string> handle_EachPage(string pageName, System.Collections.Specialized.StringDictionary args, bool faceIdModel, int delay)
        {
            logIt($"handle_EachPage: ++ page={pageName}");
            //string comport = $"COM{args["comport"]}";
            //string apsthome = args["apsthome"];
            bool supervisor = false;
            if (args.ContainsKey("supervisor"))
            {
                supervisor = true;
            }
            Random r = new Random(System.Diagnostics.Process.GetCurrentProcess().Id);
            //string tool = System.IO.Path.Combine(apsthome, @"pst\apple\MobileQKeyInput.exe");

            int modelType = PHONE_MODEL;
            if (faceIdModel)
            {
                modelType = FACE_ID_PHONE_MODEL;
               
            }
            if (args.ContainsKey("Model") && args["Model"].StartsWith("iPad", StringComparison.CurrentCultureIgnoreCase))
            {
                modelType = PAD_MODEL;
            }

            // declare and define the EnterHomeSteps object
            EnterHomeSteps enterHomeSteps = null;
            switch (modelType)
            {
                case PHONE_MODEL:
                    enterHomeSteps = new PhoneEnterHomeSteps(sIOSKeyInput, r);
                    break;
                case FACE_ID_PHONE_MODEL:
                    enterHomeSteps = new FaceIdPhoneEnterHomeSteps(sIOSKeyInput, r, delay);
                    break;
                case PAD_MODEL:
                    enterHomeSteps = new PadEnterHomeSteps(sIOSKeyInput, r);
                    break;
            }

            string next_page = "";
            try
            {
 /*               if (string.Compare(pageName, EnterHomeSteps.FIRST_PAGE, true) == 0)
                {
                    next_page = enterHomeSteps.HandleFirstPageController();
                }
                else if (string.Compare(pageName, EnterHomeSteps.RESTORE_COMPLETED_PAGE, true) == 0)
                {
                    // this is the first page, just click "Restore Complete" button.
                    next_page = enterHomeSteps.HandleRestoreCompletedPage(supervisor);
                }
                else if (string.Compare(pageName, EnterHomeSteps.BUDDY_MESA_ENROLLMENT_CONTROLLER, true) == 0)
                {
                    next_page = enterHomeSteps.HandleBuddyMesaEnrollmentController();
                }
                else if (string.Compare(pageName, EnterHomeSteps.BUDDY_PASSCODE_CONTROLLER, true) == 0)
                {
                    // this is Passcode page
                    //util.runExe(tool, $"-port={comport} -char=0x50", out err);
                    //System.Threading.Thread.Sleep(3000);
                    next_page = enterHomeSteps.HandleBuddyPasscodeController();
                }
                else if (string.Compare(pageName, EnterHomeSteps.BUDDY_IOS_TC_CONTROLLER, true) == 0)
                {
                    // end user linsence aggrement page
                    next_page = enterHomeSteps.HandleBuddyiOSTCController();
                }
                else if (string.Compare(pageName, EnterHomeSteps.BUDDY_LOCATION_SERVICES_CONTROLLER, true) == 0)
                {
                    // location page
                    next_page = enterHomeSteps.HandleBuddyLocationServicesController();
                }
                else if (string.Compare(pageName, EnterHomeSteps.BUDDY_FINISHED_CONTROLLER, true) == 0)
                {
                    // finish page
                    next_page = enterHomeSteps.HandleBuddyFinishedController();
                }
                else if (string.Compare(pageName, EnterHomeSteps.OTHER_CONTROLLERS, true) == 0)
                {
                    // face Id model and pad model
                    next_page = enterHomeSteps.HandleOtherControllers();
                }
*/
//*

                if (string.Compare(pageName, "FirstPage", true) == 0)
                {
                    // welcome page
                    if (faceIdModel)
                    {
                        sIOSKeyInput.sendKey(0x4a);
                    }
                    else
                    {
                        sIOSKeyInput.sendKey(0x52);
                    }
                    System.Threading.Thread.Sleep(2500);
                    next_page = "RestoreCompletedPage";
                }
                else if (string.Compare(pageName, "RestoreCompletedPage", true) == 0)
                {
                    // this is the first page, just click "Restore Complete" button.
                    System.Threading.Thread.Sleep(5000); //wait for wifi
                    sIOSKeyInput.sendKey(0x52);
                    System.Threading.Thread.Sleep(2500); //wait for wifi
                    if (supervisor)
                    {
                        next_page = "BuddyFinishedController";
                    }
                    else
                    {
                        next_page = "BuddyMesaEnrollmentController";
                    }
                    next_page = "BuddyWiFiController";
                    //next_page = "LocationPageController";
                    // next page: Wi-Fi selection page ??
                }
                else if (string.Compare(pageName, "BuddyWiFiController", true) == 0)
                {
                    bHasAutoJoinWifi = false;
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x52);
                    System.Threading.Thread.Sleep(2500);
                    sIOSKeyInput.sendKey(0x52);
                    System.Threading.Thread.Sleep(2500);
                    if (supervisor)
                    {
                        next_page = "BuddyFinishedController";
                    }
                    else
                    {
                        next_page = "BuddyMesaEnrollmentController";
                    }
                }
                else if (string.Compare(pageName, "BuddyMesaEnrollmentController", true) == 0)
                {
                    if (faceIdModel)
                    {
                        // this is FaceID page
                        sIOSKeyInput.sendKey(0x4f);
                        System.Threading.Thread.Sleep(500);
                        sIOSKeyInput.sendKey(0x4f);
                        System.Threading.Thread.Sleep(500);
                        sIOSKeyInput.sendKey(0x4f);
                        System.Threading.Thread.Sleep(500);
                        sIOSKeyInput.sendKey(0x52);
                        System.Threading.Thread.Sleep(2500);
                    }
                    else
                    {
                        // this is FingerPrint page
                        sIOSKeyInput.sendKey(0x4f);
                        System.Threading.Thread.Sleep(500);
                        sIOSKeyInput.sendKey(0x4f);
                        System.Threading.Thread.Sleep(500);
                        sIOSKeyInput.sendKey(0x52);
                        System.Threading.Thread.Sleep(2500);
                        sIOSKeyInput.sendKey(0x52);
                        System.Threading.Thread.Sleep(2500);
                    }
                    next_page = "BuddyPasscodeController";
                }
                else if (string.Compare(pageName, "BuddyPasscodeController", true) == 0)
                {
                    // this is Passcode page
                    sIOSKeyInput.sendKey(0x48);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x48);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    //util.runExe(tool, $"-port={comport} -char=0x50", out err);
                    //System.Threading.Thread.Sleep(3000);
                    sIOSKeyInput.sendTap();
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendTap();
                    sIOSKeyInput.sendTap();
                    next_page = "BuddyiOSTCController";
                }
                else if (string.Compare(pageName, "BuddyiOSTCController", true) == 0)
                {
                    // end user linsence aggrement page
                    System.Threading.Thread.Sleep(2500);
                    sIOSKeyInput.sendKey(0x50);
                    System.Threading.Thread.Sleep(1000);
                    sIOSKeyInput.sendTap();
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(1500);
                    sIOSKeyInput.sendTap();
                    next_page = "BuddyLocationServicesController";
                }
                else if (string.Compare(pageName, "BuddyLocationServicesController", true) == 0)
                {
                    // location page
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendKey(0x4f);
                    System.Threading.Thread.Sleep(500);
                    sIOSKeyInput.sendTap();
                    if (faceIdModel)
                    {
                        next_page = "FaceIdModelOtherControllers";
                    }
                    else
                    {
                        next_page = "BuddyFinishedController";
                    }
                }
                else if (string.Compare(pageName, "BuddyFinishedController", true) == 0)
                {
                    // finish page
                    if (faceIdModel)
                    {
                        if (bScriptSkipFinish)
                        {
                            ScriptTask script = new ScriptTask();
                            String scciptpath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "EnterAppMQ.xml");
                            script.init(scciptpath, "skiptouchscreenFinished");
                            script.doTask(sIOSKeyInput);
                        }
                        else
                        {
                            //// must send double "home" key quickly
                            String sjsonDelay = AppleSwitchControl.Properties.Settings.Default.DbClickInterval;
                            var serializer = new JavaScriptSerializer();
                            var jsondelay = (Dictionary<string, Object>)serializer.DeserializeObject(sjsonDelay);
                            List<int> intervals = null;
                            if (jsondelay.ContainsKey("iDevice"))
                            {
                                var ideviceconf = (Dictionary<string, Object>)jsondelay["iDevice"];
                                if (ideviceconf.ContainsKey(args["Model"]))
                                {
                                    intervals = ((Object[])ideviceconf[_args.Parameters["Model"]]).Cast<int>().ToList();
                                }
                            }
                            if (intervals != null || intervals.Count() > 0)
                            {
                                foreach (var iv in intervals)
                                {
                                    sIOSKeyInput.sendKey(0x48);
                                    System.Threading.Thread.Sleep(1000);

                                    sIOSKeyInput.sendKey(0x4a);
                                    System.Threading.Thread.Sleep(iv);
                                    sIOSKeyInput.sendKey(0x4a);

                                    System.Threading.Thread.Sleep(6000);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < 5; i++)
                                {// must send double "home" key quickly
                                    sIOSKeyInput.sendKey(0x48);
                                    System.Threading.Thread.Sleep(1000);

                                    sIOSKeyInput.sendKey(0x4a);
                                    System.Threading.Thread.Sleep(delay * (i + 1));
                                    sIOSKeyInput.sendKey(0x4a);

                                    System.Threading.Thread.Sleep(6000);

                                }
                            }
                        }
                    }
                    else
                    {
                        sIOSKeyInput.sendKey(0x52);
                    }
                    System.Threading.Thread.Sleep(2500);
                    next_page = "EnterHomePageDone";
                }
                else if (string.Compare(pageName, "FaceIdModelOtherControllers", true) == 0)
                {
                    // this clause is for face ID model only
                    handleFaceIdModelOtherPages(r);
                    next_page = "BuddyFinishedController";
                }
//*/
            }
            catch (Exception e)
            {
                logIt(e.Message);
                logIt(e.StackTrace);
                next_page = "EnterHomePageFailed";
            }
            logIt($"handle_EachPage: -- next page={next_page}");
            return new Tuple<DateTime, string>(DateTime.Now, next_page);
        }

        static int handle_Setttings(int steps, int settingssteps, int trust, Dictionary<string, object> devconf, StringDictionary args)
        {
            //sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(2000);

            //if (faceIdModel)
            {
                System.Threading.Thread.Sleep(2000);
                sIOSKeyInput.sendNextCount(1);
                System.Threading.Thread.Sleep(2000);

                enter_Settings_scroll(false, 1);
                enter_Settings_scroll(false, 1);

                goto_Page_First();
            }

            //enter settings
            sIOSKeyInput.sendNextCount(1);
            int nScrol = 1;
            if (devconf.ContainsKey("generalscroll"))
            {
                nScrol = Convert.ToInt32(devconf["generalscroll"]);
            }
            enter_Settings_scroll(false, nScrol);

            sIOSKeyInput.sendNextCount(settingssteps);
            Thread.Sleep(1000);
            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(4000);

            //General
            sIOSKeyInput.sendNextCount(1);

            if (devconf.ContainsKey("pmscroll"))
            {
                nScrol = Convert.ToInt32(devconf["pmscroll"]);
            }
            enter_Settings_scroll(false, nScrol);

            //sIOSKeyInput.sendNextCount(8);
            bool bnext = true;
            if (devconf.ContainsKey("pmnext"))
            {
                bnext = String.Compare(devconf["pmnext"].ToString(), "true", true) == 0;
            }
            if (bnext)
            {
                sIOSKeyInput.sendNextCount(steps);
            }
            else
            {
                sIOSKeyInput.sendPreviousCount(steps);
            }

            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(6000);

            //select app
            sIOSKeyInput.sendNextCount(trust);

            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(5000);

            //trust app
            sIOSKeyInput.sendNextCount(1);

            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(5000);

            //Dialog trust
            sIOSKeyInput.sendNextCount(1);

            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(5000);
            if (!bHasAutoJoinWifi)
            {//some time pop
                sIOSKeyInput.sendKey(0x52);
                System.Threading.Thread.Sleep(5000);
            }

            //for(int i= 0; i < 3; i++)   //goto settings
            //    sIOSKeyInput.sendTap(3000);

            Kill_one_app();

            sIOSKeyInput.sendHome(5000);

            int rapp = runApp(args);
            
            if(_args.IsParameterTrue("turnoffwifi") && bHasAutoJoinWifi)
            {
                ScriptTask script = new ScriptTask();
                String scciptpath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "EnterAppMQ.xml");
                script.init(scciptpath, "clearrunapp");
                for(int i = 0; i <= rapp; i++)
                {
                    script.doTask(sIOSKeyInput);
                }


                if (jsonconfig.ContainsKey(ModalKey))
                {
                    //Dictionary<string, object> devconf = ((Dictionary<string, object>)jsonconfig[ModalKey]);
                   // Dictionary<string, object> devconf = new Dictionary<string, object>();
                    //MergeJsonParent(devconf, ModalKey);

                    object objsettings = devconf["settings"];
                    var settings = (Dictionary<string, object>)(objsettings);
                    GoToSettings(settings);

                    var swifi = devconf.ContainsKey("wifi") ? devconf["wifi"] : null;
                    if (swifi != null)
                    {
                        int nwifi = Convert.ToInt32(swifi);
                        sIOSKeyInput.sendNextCount(nwifi);

                        sIOSKeyInput.sendKey(0x52);
                        System.Threading.Thread.Sleep(10000);

                        sIOSKeyInput.sendNextCount(1);
                        System.Threading.Thread.Sleep(1500);

                        sIOSKeyInput.sendKey(0x52);
                        System.Threading.Thread.Sleep(5000);
                    }
                }

            }

            ExitSwitchControl(devconf, args);

            return 0;
        }

        private static void ExitSwitchControl(Dictionary<string, object> devconf, StringDictionary args)
        {
            //sIOSKeyInput.sendHome(5000);
            sIOSKeyInput.sendKey(0x46);
            System.Threading.Thread.Sleep(5000);
            sIOSKeyInput.sendNextCount(1, 1500);
            sIOSKeyInput.sendKey(0x52);

            if (devconf.ContainsKey("done"))
            {
                object objsettings = devconf["done"];
                var settings = (Dictionary<string, object>)(objsettings);
                if (settings.ContainsKey("before"))
                {
                    RunExeConfig(settings["before"], args);
                }

                if (settings.ContainsKey("after"))
                {
                    RunExeConfig(settings["after"], args);
                }
            }
        }

        private static void handleFaceIdModelOtherPages(Random r)
        {
            // manual page: "True Tone", "Go Home", "Recent Apps" and "Control Center"
            trueTonePage(r);
            goHomePage(r);
            recentAppsPage(r);
            controlCenterPage(r);
        }

        private static void trueTonePage(Random r)
        {
            sIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(3000);
            sIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(3000);
            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(r.Next(5000, 8000));
        }

        private static void goHomePage(Random r)
        {
            sIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(3000);
            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(r.Next(5000, 8000));
        }

        private static void recentAppsPage(Random r)
        {
            sIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(3000);
            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(r.Next(5000, 8000));
        }

        private static void controlCenterPage(Random r)
        {
            sIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(3000);
            sIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(r.Next(5000, 8000));
        }
    }
}
