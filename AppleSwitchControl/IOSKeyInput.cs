﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AppleSwitchControl
{
    /**
     *  iosKeyinput.cs of aPhone and MobileQKeyInput.cs 
     */

    /*
# Accessibility shortcut: 0x46 (Keyboard)(PrintScreen)
# Control shortcut: 0x47 (Keyboard)(Scroll lock)
# stop scan: 0x48 (Keyboard) -- funtion disable show the blue block on screen(Pause)
# AppSwitch: 0x4d (Keyboard End)(End)
# Home: 0x4a (Keyboard Home)(Home)
# Previous item: 0x50 (Keyboard LeftArrow)
# Next item: 0x4f (Keyboard RightArrow)
# Select item: 0x51 (Keyboard DownArrow)
# Tap: 0x52 (Keyboard UpArrow)
# Scanner Menu: 0x29(Escape)
- with switch setting group off
- with scan mode to manual
- with auto hide disabled.
*/
    class IOSKeyInput
    {

        private SerialPort mSerialPort;

        public IOSKeyInput() { }

        public bool keyEventInputSerialPortOn(string serialPort)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(serialPort))
            {
                try
                {
                    mSerialPort = new SerialPort(serialPort, 9600);
                    mSerialPort.Parity = Parity.None;
                    mSerialPort.StopBits = StopBits.One;
                    mSerialPort.DataBits = 8;
                    mSerialPort.Handshake = Handshake.None;
                    mSerialPort.RtsEnable = true;
                    mSerialPort.DtrEnable = true;
                    mSerialPort.ReadTimeout = 1000;
                    mSerialPort.WriteTimeout = 1000;

                    mSerialPort.Open();
                    result = mSerialPort.IsOpen;

                }
                catch (Exception e)
                {
                    Program.logIt(e.Message);
                    Program.logIt(e.StackTrace);
                }
                Program.logIt("serial port " + mSerialPort.PortName + " on " + result);
            }
            return result;
        }

        public bool keyEventInputSerialPortOff()
        {
            bool result = false;
            if (null != mSerialPort && mSerialPort.IsOpen)
            {
                mSerialPort.Close();
                mSerialPort = null;
                result = true;
            }
            Program.logIt("serial port off " + result);
            return result;
        }

        private bool sendKeyByte(byte ch)
        {
            try
            {
                if (mSerialPort != null && mSerialPort.IsOpen)
                {
                    byte[] b = new byte[5];
                    b[0] = 0x83;
                    b[1] = 0x00;
                    b[2] = 0x00;
                    b[3] = ch;
                    b[4] = 0x00;
                    mSerialPort.Write(b, 0, 4);
                    Thread.Sleep(10);
                    mSerialPort.Write(b, 4, 1);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception )
            {
                return false;
            }
        }

        public void sendKey(byte ch)
        {
            int count = 0;
            while (true)
            {
                if (sendKeyByte(ch))
                {
                    Program.logIt($"sendKey: {ch.ToString("X2")}, result=true");
                    return;
                }
                else
                {
                    count++;
                    if (count > 3) throw new Exception();
                    Program.logIt("sendKey retry count " + count);
                }
            }
        }

        public void sendNextCount(int steps, int interval=150, int delay=500)
        {
            for(int i = 0; i < steps; i++)
            {
                sendKey(0x4f);
                Thread.Sleep(interval);
            }
            Thread.Sleep(delay);
        }

        public void sendPreviousCount(int steps, int interval = 150, int delay = 500)
        {
            for (int i = 0; i < steps; i++)
            {
                sendKey(0x50);
                Thread.Sleep(interval);
            }
            Thread.Sleep(delay);
        }

        public void sendTap(int interval = 2500)
        {
            sendKey(0x52);
            Thread.Sleep(interval);
        }

        public void sendHome(int interval = 1500)
        {
            sendKey(0x4a);
            Thread.Sleep(interval);
        }
    }
}
