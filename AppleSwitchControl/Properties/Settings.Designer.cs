﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppleSwitchControl.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfString xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <string>iPhone10,3</string>
  <string>iPhone10,6</string>
  <string>iPhone11,2</string>
  <string>iPhone11,4</string>
  <string>iPhone11,6</string>
  <string>iPhone11,8</string>
</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection faceModel {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["faceModel"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("{\r\n  \"iPhone10,3\":100,\r\n  \"iPhone10,6\":100,\r\n  \"iPhone11,2\":100,\r\n  \"iPhone11,4\":" +
            "100,\r\n  \"iPhone11,6\":100,\r\n  \"iPhone11,8\":100\r\n}")]
        public string DbClickInterval {
            get {
                return ((string)(this["DbClickInterval"]));
            }
        }
    }
}
