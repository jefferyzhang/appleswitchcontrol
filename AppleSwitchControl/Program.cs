﻿using FDTools;
using iDeviceRestoreBackup;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using utility;

namespace AppleSwitchControl
{
    partial class Program
    {
        static String TAG = "[AppleSwitchControl]";

        private static IOSKeyInput sIOSKeyInput = null;
        private static String ModalKey = "";
        private static String ModaldefaultKey = "";
        private static Dictionary<string, object> jsonconfig=null; //json config

        //config data define:
        private static int nHomeScrollMenu = 4; //go n steps to Scroll menu
        public static Boolean bScriptSkipFinish = true;

        public static void logIt(String msg)
        {
            System.Diagnostics.Trace.WriteLine($"{TAG}: {msg}");
        }

        private static void LoadJsonConfig()
        {
            if (jsonconfig == null)
            {
                String sConfig = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "appleswitchcontrol.json");
                var serializer = new JavaScriptSerializer();
                bool bCreateNew;
                Mutex lockxml = new Mutex(false, Path.GetFileName(sConfig), out bCreateNew);
                //if (bCreateNew)
                {
                    lockxml.WaitOne();
                    try
                    {
                        jsonconfig = (Dictionary<string, object>)serializer.DeserializeObject(File.ReadAllText(sConfig));
                    }
                    finally
                    {
                        lockxml.ReleaseMutex();
                    }
                }
            }
        }

        private static bool establishKeyInputComPort(string comport)
        {
            sIOSKeyInput = new IOSKeyInput();
            // try 3 times
            int count = 0;
            while (true)
            {
                if (sIOSKeyInput.keyEventInputSerialPortOn(comport))
                {
                    return true;
                }
                else
                {
                    count++;
                    if (count > 3) break;
                }
            }
            sIOSKeyInput = null;
            return false;
        }

        private static void disconnectKeyInputComPort()
        {
            if (sIOSKeyInput != null)
            {
                sIOSKeyInput.keyEventInputSerialPortOff();
                sIOSKeyInput = null;
            }
        }

        static System.Collections.Generic.List<Object> _screen_icon = null;
        static Tuple<bool, int, int> find_app_icon_by_name(string app_name)
        {
            bool found = false;
            int page = -1;
            int index = -1;
            if (_screen_icon != null && !string.IsNullOrEmpty(app_name))
            {
                for (int i = 0; i < _screen_icon.Count && !found; i++)
                {
                    System.Collections.Generic.List<Object> icons = _screen_icon[i] as System.Collections.Generic.List<Object>;
                    for (int j = 0; j < icons.Count && !found; j++)
                    {
                        System.Collections.Generic.Dictionary<string, object> icon = icons[j] as System.Collections.Generic.Dictionary<string, object>;
                        if ((icon.ContainsKey("displayName") && string.Compare(app_name, icon["displayName"] as string, true) == 0) ||
                            (icon.ContainsKey("bundleIdentifier") && string.Compare(app_name, icon["bundleIdentifier"] as string, true) == 0) ||
                            (icon.ContainsKey("displayIdentifier") && string.Compare(app_name, icon["displayIdentifier"] as string, true) == 0))
                        {
                            found = true;
                            page = i;
                            index = j;
                            break;
                        }
                    }
                }
            }
            return new Tuple<bool, int, int>(found, page, index);
        }


        static String GetWifiInfo(String sPath, String sLabel, StringDictionary args)
        {
            Dictionary<String, String> wifi = new Dictionary<string, string>();
            if (File.Exists(sPath))
            {
               Object obj =  PListReadWrite.readPlist(sPath);
               if(obj is Dictionary<String, object>)
                {
                    Dictionary<String, object> item = (Dictionary<String, object>)obj;
                    if(item.ContainsKey("PayloadContent"))
                    {
                        Object arrject = item["PayloadContent"];
                        if(arrject is List<Object>)
                        {
                            List<Object> listobj = (List<Object>)arrject;
                            if (listobj.Count > 0)
                            {
                                Object wfi = listobj.First();
                                if (wfi is Dictionary<String, object>)
                                {
                                    Dictionary<String, object> wifiinfo = (Dictionary<String, object>)wfi;
                                    if (wifiinfo.ContainsKey("SSID_STR"))
                                    {
                                        wifi.Add("ssid", wifiinfo["SSID_STR"].ToString());
                                    } 
                                    if(wifiinfo.ContainsKey("Password"))
                                    {
                                        wifi.Add("password", wifiinfo["Password"].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var serializer = new JavaScriptSerializer();
            String ss = serializer.Serialize(wifi);
            String sjsonPath = Path.Combine(Environment.ExpandEnvironmentVariables($"%APSTHOME%FDTemporary\\label_{sLabel}"), "wifi.json");
            File.WriteAllText(sjsonPath, ss);
            args["wifijson"] = sjsonPath;
            return sjsonPath;
        }

        static String GetPortNameFromHub(String sLabel, String sHubName, int nPort, String ids)
        {
            String sPort="";
            Tuple<int, StringDictionary> tupleinfo = ReadUsbInfo.GetUsbDeviceInfomationByHubNameAndPort(sLabel, sHubName, nPort);
            if (tupleinfo.Item1 == 0)
            {
                if (tupleinfo.Item2.ContainsKey("driverkey"))
                {
                    String sdriverkey = tupleinfo.Item2["driverkey"];
                    RegistryKey key = Registry.LocalMachine.OpenSubKey($"SYSTEM\\CurrentControlSet\\Enum\\USB\\{ids}");
                    foreach (var v in key.GetSubKeyNames())
                    {
                        Console.WriteLine(v);
                        RegistryKey productKey = key.OpenSubKey(v);
                        if (String.Compare(sdriverkey, Convert.ToString(productKey.GetValue("Driver")), true) == 0)
                        {
                            sPort = Convert.ToString(productKey.GetValue("FriendlyName"));
                            logIt(sPort);
                            Regex reg = new Regex(@"^.*?\((.*?)\)$");
                            Match m = reg.Match(sPort);
                            if (m.Success)
                            {
                                sPort = m.Groups[1].Value;
                            }
                            else
                            {
                                sPort = "";
                            }
                        }
                    }
                }
                else
                {
                    logIt("GetUsbDeviceInfomationByHubNameAndPort can not get driver key.");
                }
            }
            else
            {
                logIt("GetUsbDeviceInfomationByHubNameAndPort fail, errorcode:" + tupleinfo.Item1);
            }
            return sPort;
        }

        static void MergeJsonParent(Dictionary<string, object> config_jason ,string Key)
        {
            if (jsonconfig.ContainsKey(Key))
            {
                Dictionary<string, object> devconf = ((Dictionary<string, object>)jsonconfig[Key]);

                if (devconf.ContainsKey("parent"))
                {
                    MergeJsonParent(config_jason, devconf["parent"].ToString());
                }
                foreach(var devd in devconf.Where(x=>!x.Key.Equals("parent")))
                {
                    if (config_jason.ContainsKey(devd.Key)){
                        config_jason[devd.Key] = devd.Value;
                    }
                    else
                    {
                        config_jason.Add(devd.Key, devd.Value);
                    }
                }

            }

        }


        static System.Configuration.Install.InstallContext _args = null;
        static int Main(string[] args)
        {
 
            _args = new System.Configuration.Install.InstallContext(null, args);
            if (_args.IsParameterTrue("test"))
            {

                establishKeyInputComPort(_args.Parameters["com"]);

                {// must send double "home" key quickly
                    sIOSKeyInput.sendKey(0x48);
                    System.Threading.Thread.Sleep(1000);
                    for (int i = 0; i < 1; i++)
                    {
                        sIOSKeyInput.sendKey(0x4a);
                        System.Threading.Thread.Sleep(Convert.ToInt32(_args.Parameters["interval"]));
                        sIOSKeyInput.sendKey(0x4a);

                        //System.Threading.Thread.Sleep(4000);
                    }
                }

                disconnectKeyInputComPort();

                return 0;
            }

            if (_args.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for debug, press any key to continue...");
                System.Console.ReadKey();
            }

            if (_args.Parameters.ContainsKey("label"))
                TAG = string.Format("[AppleSwitchControl]: [Label_{0}]", _args.Parameters["label"]);

            string self_name = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            // dump version
            System.Diagnostics.FileVersionInfo f = System.Diagnostics.FileVersionInfo.GetVersionInfo(self_name);
            logIt(string.Format("version: {0}", f.FileVersion));
            // dump command line:
            logIt("Dump command line:");
            logIt(System.Environment.CommandLine);
            // dump args:
            logIt("Dump parameters:");
            foreach (DictionaryEntry value in _args.Parameters)
            {
                logIt(string.Format("{0}={1}", value.Key, value.Value));
            }

            String sHubName = @"USB#VID_8087&PID_0024#5&364a44c3&0&1#{f18a0e88-c30c-11d0-8815-00a0c906bed8}";
            int nPort = 7;
            String sID = @"VID_1A86&PID_7523";

            if (_args.Parameters.ContainsKey("dongleid"))
                sID = _args.Parameters["dongleid"];


            if (_args.Parameters.ContainsKey("hubname"))
                sHubName =  _args.Parameters["hubname"];

            if (_args.Parameters.ContainsKey("hubport"))
            {
                try
                {
                    nPort = Convert.ToInt32(_args.Parameters["hubport"]);
                }
                catch(Exception e)
                {
                    Program.logIt(e.Message);
                    Program.logIt(e.StackTrace);
                    return -1;
                }
            }

            if (_args.Parameters.ContainsKey("info"))
            {
                if(File.Exists(Environment.ExpandEnvironmentVariables(_args.Parameters["info"])))
                {
                    IniFile ini = new IniFile(Environment.ExpandEnvironmentVariables(_args.Parameters["info"]));
                    String sTemp = ini.GetString("info", "ProductType", "");
                    try
                    {
                        _args.Parameters.Add("Model", sTemp);
                    }
                    catch (Exception) { }
                    sTemp = ini.GetString("info", "ProductVersion", "");
                    try
                    {
                        _args.Parameters.Add("Version", sTemp);
                    }
                    catch (Exception) { }

                }
                else {
                    logIt($"{_args.Parameters["info"]} file not exist.");
                    return 101;
                }
            }
            
            try
            {
                if (String.IsNullOrEmpty(_args.Parameters["Model"]) || String.IsNullOrEmpty(_args.Parameters["Version"]))
                {
                    logIt($"Model or version info is less.");
                    return 115;
                }
                else
                {
                    ModalKey = _args.Parameters["Model"] + "_" + _args.Parameters["Version"];
                    ModaldefaultKey = _args.Parameters["Model"] + "_default";
                    if (ModalKey.Length < 2)
                    {
                        logIt($"{ModalKey} info is less.");
                        return 103;
                    }
                }
            }
            catch (Exception) { return 102; }

            String sComport;
            if (_args.Parameters.ContainsKey("com"))
            {
                String sss = _args.Parameters["com"];
                sComport = sss.StartsWith("COM",StringComparison.CurrentCultureIgnoreCase)? sss : $"COM{_args.Parameters["com"]}";
            }
            else
            {
                sComport = GetPortNameFromHub(_args.Parameters.ContainsKey("label") ? _args.Parameters["label"] : "0", sHubName, nPort, sID);
            }

            if (_args.Parameters.ContainsKey("pathprofile"))
            {
                GetWifiInfo(_args.Parameters["pathprofile"], _args.Parameters["label"], _args.Parameters);
            }

            //Check Ring
            DateTime dtStat = DateTime.Now;
            while ((DateTime.Now - dtStat).TotalMinutes < 10)
            {
                Tuple<int, int> res1 = FDTools.SerialPortHelper.GetModemStatusByComPort(sComport);
                if (res1.Item1 == 0)
                {
                    Thread.Sleep(1500);
                    // read comport success
                    if ((res1.Item2 & 0x40) == 0x00)
                    {
                        break;
                    }

                }
            }

            establishKeyInputComPort(sComport);

            #region test script 
            if (_args.Parameters.ContainsKey("script"))
            {
                ScriptTask script = new ScriptTask();
                String scciptpath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "EnterAppMQ.xml");
                script.init(scciptpath, "com.radi.app");
                script.doTask(sIOSKeyInput);
                disconnectKeyInputComPort();
                return 0;
            }
            #endregion 


            int settingsdown = 5;
            int trust = 1;
            int profiledevicemanage = 8;

            try
            {
                LoadJsonConfig();
                if (!jsonconfig.ContainsKey(ModalKey))
                {
                    ModalKey = ModaldefaultKey;
                }

                if (jsonconfig.ContainsKey(ModalKey))
                {
                    Dictionary<string, object> devconf = new Dictionary<string, object>();
                    MergeJsonParent(devconf, ModalKey);

                    List<object> pages = devconf.ContainsKey("startpages") ? ((object[])devconf["startpages"]).ToList() : null;
                    if (!_args.IsParameterTrue("skipsetup"))
                    {
                        //
                        enter_homepage_v2(_args.Parameters, pages);
                        Thread.Sleep(2000);
                    }

                    var sHomeScrollMenu = devconf.ContainsKey("homescrollmenu") ? devconf["homescrollmenu"] : null;
                    if(sHomeScrollMenu != null)
                        nHomeScrollMenu = Convert.ToInt32(sHomeScrollMenu);

                    var sScriptSkipFinish = devconf.ContainsKey("scriptskipfinish") ? devconf["scriptskipfinish"] : null;
                    if(sScriptSkipFinish != null)
                        bScriptSkipFinish = Convert.ToInt32(sScriptSkipFinish)==1;


                    object objsettings = devconf["settings"];
                    var settings = (Dictionary<string, object>)(objsettings);
                    GoToSettings(settings);

                    var general = devconf.ContainsKey("general")? devconf["general"]:null;
                    if(general!=null)
                        settingsdown = Convert.ToInt32(general);

                    var ssPM = devconf.ContainsKey("pm") ? devconf["pm"] : null;
                    if (ssPM != null)
                        profiledevicemanage = Convert.ToInt32(ssPM);

                    var strust = devconf.ContainsKey("trust") ? devconf["trust"] : null;
                    if(strust!=null)
                        trust = Convert.ToInt32(strust);

                    handle_Setttings(profiledevicemanage, settingsdown, trust, devconf, _args.Parameters);

                }
                else
                {
                    logIt($"json to settings failed.");
                    return 105;
                }


            }catch(Exception e)
            {
                Program.logIt(e.Message);
                Program.logIt(e.StackTrace);
                return -2;
            }
            disconnectKeyInputComPort();

            return 0;

        }
    }
}
