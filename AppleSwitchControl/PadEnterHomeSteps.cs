﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleSwitchControl
{
    class PadEnterHomeSteps : PhoneEnterHomeSteps
    {
        public PadEnterHomeSteps(IOSKeyInput iOSKeyInput, Random random) : base(iOSKeyInput, random)
        {
        }

        public override string HandleBuddyMesaEnrollmentController()
        {
            // iPad doesn't have this controller
            return BUDDY_PASSCODE_CONTROLLER;
        }

        public override string HandleBuddyLocationServicesController()
        {
            base.HandleBuddyLocationServicesController();
            return OTHER_CONTROLLERS;
        }

        public override string HandleOtherControllers()
        {
            quickAccessToDock();
            recentAppsPage();
            controlCenterPage();
            return BUDDY_FINISHED_CONTROLLER;
        }

        private void quickAccessToDock()
        {
            mIOSKeyInput.sendKey(0x50);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }

        private void recentAppsPage()
        {
            mIOSKeyInput.sendKey(0x50);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }

        private void controlCenterPage()
        {
            mIOSKeyInput.sendKey(0x50);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
        }
    }
}
