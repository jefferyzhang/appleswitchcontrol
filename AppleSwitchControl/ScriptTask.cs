﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace AppleSwitchControl
{
    class StepInfo
    {
        public String sChar;
        public int sLeep; //default = 250;
        public int count;  // 0
    }
    class ScriptTask
    {

        private List<StepInfo> lstStepInfos;

        private void GetFunctions(List<StepInfo> lstSte, XDocument xml, String sID)
        {
            var task = (from e in xml.Root.Elements("functions").Elements("funtion")
                        where (string)e.Attribute("id") == sID
                        select e).First();
            var items = task.Elements().Where(x => x.NodeType != XmlNodeType.Comment);

            foreach (var item in items)
            {
//                Program.logIt(item.ToString());
                if (item.Name == "key")
                {
                    StepInfo si = new StepInfo();
                    si.sChar = (String)item.Attribute("char")??"";
                    si.sLeep = item.Attribute("sleep")!=null ? Convert.ToInt32((string)item.Attribute("sleep")) : 250;
                    si.count = item.Attribute("count") != null ? Convert.ToInt32((string)item.Attribute("count")) : 1;
                    lstSte.Add(si);
                }
                else if(item.Name == "funtion")
                {
                    GetFunctions(lstSte, xml, (String)item.Attribute("id"));
                }
                else
                {
                    Program.logIt("xml format error:" + item.ToString());
                }
            }

        }

        //private XDocument xml;
        public void init(String sPath, String sID)
        {
            lstStepInfos = new List<StepInfo>();
            bool bCreateNew;
            Mutex lockxml = new Mutex(false, Path.GetFileName(sPath), out bCreateNew);
            lockxml.WaitOne();
            try
            {
                XDocument xml = XDocument.Load(sPath);
                var task = (from e in xml.Root.Elements("tasks").Elements("task")
                            where (string)e.Attribute("id") == sID
                            select e).First();
                var items = task.Elements().Where(x => x.NodeType != XmlNodeType.Comment);

                foreach (var item in items)
                {
                    if (item.Name == "key")
                    {
                        StepInfo si = new StepInfo();
                        si.sChar = (String)item.Attribute("char") ?? "";
                        si.sLeep = item.Attribute("sleep") != null ? Convert.ToInt32((string)item.Attribute("sleep")) : 250;
                        si.count = item.Attribute("count") != null ? Convert.ToInt32((string)item.Attribute("count")) : 1;
                        lstStepInfos.Add(si);
                    }
                    else if (item.Name == "funtion")
                    {
                        GetFunctions(lstStepInfos, xml, (String)item.Attribute("id"));
                    }
                    else
                    {
                        Program.logIt("xml format error:" + item.ToString());
                    }
                }
               
            }
            finally
            {
                lockxml.ReleaseMutex();
            }
            Program.logIt(lstStepInfos.ToString());
        }


        public Boolean doTask(IOSKeyInput input)
        {
            foreach(var item in lstStepInfos)
            {
                if (String.IsNullOrEmpty(item.sChar))
                {
                    for(int i  = 0; i < item.count; i++)
                    {
                        Thread.Sleep(item.sLeep);
                    }
                }
                else
                {
                    byte bcmd = Convert.ToByte(item.sChar, 16);                  
                    for (int i = 0; i < item.count; i++)
                    {
                        input.sendKey(bcmd);
                        Thread.Sleep(item.sLeep);
                    }
                }
            }

            return true;
        }

    }
}
