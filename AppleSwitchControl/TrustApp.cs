﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AppleSwitchControl
{
    partial class Program
    {
        const string quote = "\"";
        static public List<string> runEXE(string exeFilename, string args, out uint exitCode, int timeout = 100 * 1000)
        {
            List<string> ret = new List<string>();
            exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, args));
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = args;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;

                    p.OutputDataReceived += (s, o) =>
                    {
                        if (o.Data == null)
                            ev.Set();
                        else
                        {
                            logIt(string.Format("[runExe]: {0}", o.Data));
                            ret.Add(o.Data);
                        }
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        ev.WaitOne(timeout);
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = (uint)p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                        logIt(string.Format("[runExe]: Kill by caller due to timeout={0}", timeout));
                    }
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return ret;
        }


        static int enter_Settings_scroll(Boolean bup = true, int nTime=1)
        {
            if (nTime == 0) return 0;
            sIOSKeyInput.sendKey(0x51);
            Thread.Sleep(500);
            sIOSKeyInput.sendNextCount(2);
            sIOSKeyInput.sendKey(0x52);
            Thread.Sleep(1000);
            int gonext = bup ? 2 : 1;
            sIOSKeyInput.sendNextCount(gonext);
            for (int i = 0; i < nTime; i++)
            {
                sIOSKeyInput.sendKey(0x52);
                Thread.Sleep(1000);
            }

            sIOSKeyInput.sendPreviousCount(gonext + 1);
            sIOSKeyInput.sendKey(0x51);
            Thread.Sleep(1000);

            return 0;
        }

        static int enter_Home_scroll(Boolean right = true)
        {
            sIOSKeyInput.sendKey(0x51);
            Thread.Sleep(500);

            sIOSKeyInput.sendNextCount(nHomeScrollMenu);
            sIOSKeyInput.sendKey(0x52);
            Thread.Sleep(1000);

            int gonext = right ? 2 : 1;
            sIOSKeyInput.sendNextCount(gonext);
            sIOSKeyInput.sendKey(0x52);
            Thread.Sleep(1000);

            sIOSKeyInput.sendPreviousCount(gonext + 1);
            sIOSKeyInput.sendKey(0x51);
            Thread.Sleep(1000);

            return 0;
        }

        static int goto_Page_First()
        {
            sIOSKeyInput.sendKey(0x51);
            Thread.Sleep(500);

            sIOSKeyInput.sendNextCount(2);
            sIOSKeyInput.sendKey(0x52);
            Thread.Sleep(1000);

            int gonext = 3;
            sIOSKeyInput.sendNextCount(gonext);
            sIOSKeyInput.sendKey(0x52);
            Thread.Sleep(1000);

            return 0;
        }

        static int Kill_one_app()
        {
            sIOSKeyInput.sendKey(0x4d);
            Thread.Sleep(2500);
           
            sIOSKeyInput.sendKey(0x51);
            Thread.Sleep(1000);

            sIOSKeyInput.sendNextCount(2);
            sIOSKeyInput.sendTap();
            return 0;
        }

        static void GoToSettings(Dictionary<string, object> settings)
        {
            if (settings != null)
            {
                var page = Convert.ToInt32(settings["page"]);
                var index = Convert.ToInt32(settings["index"]);
                if (page == 0)
                {
                    int steps = 4 - index;
                    sIOSKeyInput.sendPreviousCount(steps, 250);
                }
                else
                {
                    for (int i = 1; i < page; i++)
                    {
                        enter_Home_scroll();
                    }
                    sIOSKeyInput.sendNextCount(index, 150);
                    Thread.Sleep(200);

                    sIOSKeyInput.sendKey(0x52);
                    System.Threading.Thread.Sleep(5000);

                }
            }
            Thread.Sleep(2000);
        }

        static int RunExeConfig(Object obj, StringDictionary args)
        {
            uint iRet = 110;
            if(obj!=null && (obj is Dictionary<string, object>))
            {
                Dictionary<string, object> ii = (Dictionary<string, object>)obj;
                if (ii.ContainsKey("cmd"))
                {
                    String exe = Environment.ExpandEnvironmentVariables(ii["cmd"].ToString());
                    string sParam = null;
                    if (ii.ContainsKey("param"))
                    {
                        sParam = Environment.ExpandEnvironmentVariables(ii["param"].ToString());
                        foreach (DictionaryEntry sk in args)
                        {
                            if (sParam.IndexOf($"={sk.Key}") > 0)
                            {
                                sParam = sParam.Replace($"={sk.Key}", "="+quote + Environment.ExpandEnvironmentVariables(sk.Value.ToString().Trim('\"')) + quote);
                            }
                        }
                        //sParam = sParam.Replace("{udid}", args["udid"]);
                        //if (sParam.IndexOf("{pathprofile}") > 0)
                        //{
                        //    sParam = sParam.Replace("{pathprofile}", quote+ Environment.ExpandEnvironmentVariables(args["pathprofile"])+ quote);
                        //}
                    }
                    int nTimeOut = 1 * 60 * 1000;
                    if (ii.ContainsKey("timeout"))
                    {
                        nTimeOut = Convert.ToInt32(ii["timeout"]);
                    }
                    runEXE(exe, sParam, out iRet, nTimeOut);
                }
                else
                {
                    logIt("with out cmd key");
                    iRet = 111;
                }
            }
            else
            {
                logIt("Not need run command.");
            }
            return (int)iRet;
        }

        static int  runApp(StringDictionary args)
        {
            int retcnt = 0;
            Dictionary<string, object> devconf = new Dictionary<string, object>();
            try
            {
                if (jsonconfig.ContainsKey(ModalKey))
                {
                    MergeJsonParent(devconf, ModalKey);
                    var apps = from x in devconf
                               where x.Key.Contains("com.")
                               select x;
                    foreach (var x in apps)
                    {
                        retcnt++;
                        object objsettings = x.Value;
                        var settings = (Dictionary<string, object>)(objsettings);
                        if (settings.ContainsKey("before"))
                        {
                            RunExeConfig(settings["before"], args);
                        }
                        var page = Convert.ToInt32(settings["page"]);
                        var index = Convert.ToInt32(settings["index"]);
                        if (page == 0)
                        {
                            int steps = 4 - index;
                            sIOSKeyInput.sendPreviousCount(steps, 250);
                        }
                        else
                        {
                            for (int i = 1; i < page; i++)
                            {
                                enter_Home_scroll();
                            }
                            sIOSKeyInput.sendNextCount(index, 250);
                            Thread.Sleep(200);
                        }

                        //For App OK
                        sIOSKeyInput.sendTap();
                        Thread.Sleep(5000);

                        var task = settings.ContainsKey("taskid") ? settings["taskid"] : null;
                        if (task != null)
                        {
                            ScriptTask script = new ScriptTask();
                            String scciptpath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "EnterAppMQ.xml");
                            script.init(scciptpath, task.ToString());
                            script.doTask(sIOSKeyInput);
                        }

                        if (settings.ContainsKey("after"))
                        {
                            RunExeConfig(settings["after"], args);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Program.logIt(e.Message);
                Program.logIt(e.StackTrace);
            }
            return retcnt;
        }
    }
}
