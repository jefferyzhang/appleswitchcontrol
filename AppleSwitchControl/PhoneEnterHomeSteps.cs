﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleSwitchControl
{
    class PhoneEnterHomeSteps : EnterHomeSteps
    {
        protected Random mRandom;
        protected IOSKeyInput mIOSKeyInput;

        public PhoneEnterHomeSteps(IOSKeyInput iOSKeyInput, Random random)
        {
            mRandom = random;
            mIOSKeyInput = iOSKeyInput;
        }

        public override string HandleFirstPageController()
        {
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return RESTORE_COMPLETED_PAGE;
        }

        public override string HandleRestoreCompletedPage(bool supervisorMode)
        {
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            if (supervisorMode)
            {
                return BUDDY_FINISHED_CONTROLLER;
            }
            return BUDDY_MESA_ENROLLMENT_CONTROLLER;
        }

        public override string HandleBuddyWiFiController(bool supervisor)
        {
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            if (supervisor)
            {
                return BUDDY_FINISHED_CONTROLLER;
            }
            else
            {
                return BUDDY_MESA_ENROLLMENT_CONTROLLER;
            }
        }

        public override string HandleBuddyMesaEnrollmentController()
        {
            // this is FingerPrint page
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return BUDDY_PASSCODE_CONTROLLER;
        }

        public override string HandleBuddyPasscodeController()
        {
            mIOSKeyInput.sendKey(0x48);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x48);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return BUDDY_IOS_TC_CONTROLLER;
        }

        public override string HandleBuddyiOSTCController()
        {
            System.Threading.Thread.Sleep(10000);
            mIOSKeyInput.sendKey(0x50);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return BUDDY_LOCATION_SERVICES_CONTROLLER;
        }

        public override string HandleBuddyLocationServicesController()
        {
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x4f);
            System.Threading.Thread.Sleep(SAME_PAGE_STEP_INTERVAL);
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return BUDDY_FINISHED_CONTROLLER;
        }

        public override string HandleBuddyFinishedController()
        {
            mIOSKeyInput.sendKey(0x52);
            System.Threading.Thread.Sleep(PAGE_CHANGE_WAIT_READY);
            return ENTER_HOME_PAGE_DONE;
        }

    }
}
