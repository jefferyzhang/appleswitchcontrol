﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using utility;

namespace PSTLoader
{
    class Program
    {
        static String TAG = "[PSTLoader]";
        static StringDictionary lines = new StringDictionary();

        public static void logIt(String msg)
        {
            System.Diagnostics.Trace.WriteLine($"{TAG}: {msg}");
        }

        static String ExtraWifiprofile(String sLabel)
        {
            String destinationPath = System.IO.Path.Combine(Environment.ExpandEnvironmentVariables($"%APSTHOME%FDTemporary\\label_{sLabel}"), "Label_"+sLabel+ "_wifi.mobileconfig");

            String local_fn = Environment.ExpandEnvironmentVariables("%APSTHOME%mergerWifiProfile.zip");
            if (File.Exists(local_fn))
            {
                using (ZipArchive archive = ZipFile.OpenRead(local_fn))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (Path.GetExtension(entry.FullName) == ".mobileconfig")
                        {
                            entry.ExtractToFile(destinationPath, true);
                        }
                    }
                }
            }
            return destinationPath;
        }

        static Task downloadinfo(String udid, String sLabel)
        {
            Task a = Task.Factory.StartNew(() =>
            {
                if (!string.IsNullOrEmpty(udid))
                {
                    try
                    {
                        string local_dir = System.IO.Path.Combine(Environment.ExpandEnvironmentVariables($"%APSTHOME%FDTemporary\\label_{sLabel}"));
                        System.IO.Directory.CreateDirectory(local_dir);
                        string local_fn = System.IO.Path.Combine(local_dir, $"{udid}.zip");
                        //if (System.IO.File.Exists(local_fn))
                        //{
                        //    System.IO.File.Delete(local_fn);
                        //}
                        string url = "ftp://ftp2.futuredial.com/iTube/info/";
                        string username = "fd_eng";
                        string password = "FDeng";
                        using (WebClient wc = new WebClient())
                        {
                            wc.UseDefaultCredentials = true;
                            wc.Credentials = new NetworkCredential(username, password);
                            byte[] data = wc.DownloadData($"{url}{udid}.zip");
                            if (data.Length > 0)
                            {
                                System.IO.File.WriteAllBytes(local_fn, data);
                            }
                        }
                        if (PSTLoader.Properties.Settings.Default.bDeleteFtp)
                        {
                            try
                            {
                                FtpWebRequest delftpfile = (FtpWebRequest)WebRequest.Create($"{url}{udid}.zip");
                                delftpfile.Credentials = new NetworkCredential(username, password);
                                delftpfile.Method = WebRequestMethods.Ftp.DeleteFile;
                                FtpWebResponse response = (FtpWebResponse)delftpfile.GetResponse();
                                logIt($"Delete status: {response.StatusDescription}");
                                response.Close();
                            }
                            catch (Exception e)
                            {
                                logIt(e.ToString());
                                logIt(e.StackTrace);
                            }
                        }


                        using (ZipArchive archive = ZipFile.OpenRead(local_fn))
                        {
                            foreach (ZipArchiveEntry entry in archive.Entries)
                            {
                                if (Path.GetExtension(entry.FullName) == ".prop")
                                {
                                    using (StreamReader r = new StreamReader(entry.Open()))
                                    {
                                        string line = r.ReadLine();
                                        while (!string.IsNullOrEmpty(line))
                                        {
                                            int pos = line.IndexOf('=');
                                            if (pos > 0 && pos < line.Length - 1)
                                            {
                                                string k = line.Substring(0, pos);
                                                string v = line.Substring(pos + 1);
                                                if (!String.IsNullOrEmpty(v))
                                                {
                                                    if (lines.ContainsKey(k))
                                                    {
                                                        lines.Add(k, v);
                                                    }
                                                    else
                                                    {
                                                        lines[k] = v;
                                                    }
                                                }
                                            }
                                            line = r.ReadLine();
                                        }
                                    }
                                }
                                else if (Path.GetExtension(entry.FullName) ==".plist")
                                {
                                    // extra {udid}.plist to 
                                    string fn1 = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Apple", "Lockdown", $"{entry.FullName}.plist");
                                    try
                                    {
                                        if (File.Exists(fn1))
                                            System.IO.File.Delete(fn1);
                                        entry.ExtractToFile(fn1, true);
                                    }
                                    catch (Exception) { }
                                }
                            }
                        }


                        File.Delete(local_fn);
                    }
                    catch (Exception ex)
                    {
                        logIt(ex.Message);
                        logIt(ex.StackTrace);
                    }
                    SendInfotoControl(InfoToJson(lines, sLabel));
                }
            });
            return a;
        }

        static public List<string> runEXE(string exeFilename, string args, out uint exitCode, int timeout = 100 * 1000)
        {
            List<string> ret = new List<string>();
            exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, args));
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = args;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;

                    p.OutputDataReceived += (s, o) =>
                    {
                        if (o.Data == null)
                            ev.Set();
                        else
                        {
                            logIt(string.Format("[runExe]: {0}", o.Data));
                            ret.Add(o.Data);
                        }
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        ev.WaitOne(timeout);
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = (uint)p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                        logIt(string.Format("[runExe]: Kill by caller due to timeout={0}", timeout));
                    }
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return ret;
        }

        static Dictionary<String, object> InfoToJson(StringDictionary lines, String sLabel)
        {
            Dictionary<String, object> jasonobject = new Dictionary<String, object>();

            Dictionary<String, String> data = new Dictionary<string, string>();
            jasonobject.Add("label", sLabel);
            jasonobject.Add("data", data);
            data.Add($"/labelinfo/device/manufacturer", "Apple");
            if (lines.ContainsKey("ProductType"))
            {
                data.Add($"/labelinfo/device/producttype", lines["ProductType"]);
//                lines.Remove("ProductType");
            }
            if (lines.ContainsKey("InternationalMobileEquipmentIdentity"))
            {
                data.Add($"/labelinfo/device/meid_imei", lines["InternationalMobileEquipmentIdentity"]);
                data.Add($"/labelinfo/device/deviceid", lines["InternationalMobileEquipmentIdentity"]);
  //              lines.Remove("InternationalMobileEquipmentIdentity");
            }
            if (lines.ContainsKey("UniqueDeviceID"))
            {
                data.Add($"/labelinfo/device/serialnumber", lines["UniqueDeviceID"]);
 //               lines.Remove("UniqueDeviceID");
            }
            if (lines.ContainsKey("MLBSerialNumber"))
            {
                data.Add($"/labelinfo/device/iPhoneSerialNumber", lines["MLBSerialNumber"]);
//                lines.Remove("MLBSerialNumber");
            }
            if (lines.ContainsKey("ProductVersion"))
            {
                data.Add($"/labelinfo/device/productversion", lines["ProductVersion"]);
   //             lines.Remove("ProductVersion");
            }
            if (lines.ContainsKey("marketingname"))
            {
                data.Add($"/labelinfo/device/model", lines["marketingname"]);
            }
            if (lines.ContainsKey("hardwaremodel"))
            {
                data.Add($"/labelinfo/device/hardwaremodel", lines["hardwaremodel"]);
                lines.Remove("hardwaremodel");
            }
            if (lines.ContainsKey("buildversion"))
            {
                data.Add($"/labelinfo/device/buildversion", lines["buildversion"]);
                lines.Remove("buildversion");
            }
            if (lines.ContainsKey("modelnumber"))
            {
                data.Add($"/labelinfo/device/modelnumber", lines["modelnumber"]);
                data.Add($"/labelinfo/runtime/modelnumber", lines["modelnumber"]);
                lines.Remove("modelnumber");
            }

            foreach (DictionaryEntry kv in lines)
            {
                try
                {
                    data.Add($"/labelinfo/runtime/{kv.Key}", (String)kv.Value);
                }
                catch (Exception) { }
            }

            bool bAddmem = false;
            if (lines.ContainsKey("TotalDiskCapacity"))
            {
                try
                {
                    data.Add($"/labelinfo/runtime/memory", BytesAsString(Convert.ToInt64(lines["TotalDiskCapacity"])));
                    bAddmem = true;
                }
                catch (Exception) { }
            }
            if(!bAddmem && lines.ContainsKey("FSTotalBytes"))
            {
                try
                {
                    data.Add($"/labelinfo/runtime/memory", BytesAsString(Convert.ToInt64(lines["FSTotalBytes"])));
                    bAddmem = true;
                }
                catch (Exception) { }
            }

            try
            {
                data.Add($"/labelinfo/runtime/starttime", startime.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch (Exception) { }


            try
            {
                String sArgs = String.Format("-ProductType={0} -ProductVersion={1} -DeviceColor={2} -DeviceEnclosureColor={3}", lines["ProductType"], lines["ProductVersion"], lines["DeviceColor"], lines["DeviceEnclosureColor"]);
                String exeColor = Path.Combine(Environment.ExpandEnvironmentVariables(@"%apsthome%phonedll\PST_APE_UNIVERSAL_USB_FD\resource"), "iDeviceColor.exe");
                uint exitCode = 0;
                List<String> ret = runEXE(exeColor, sArgs, out exitCode, 2 * 60 * 1000);
                if (exitCode == 0)
                {
                    var color = ret.Where(x => x.StartsWith("DeviceSpecColor")).Select(x => x.Substring("DeviceSpecColor=".Length)).FirstOrDefault();
                    if(!String.IsNullOrEmpty(color))
                    {
                        try
                        {
                            data.Add($"/labelinfo/runtime/color", color);
                        }
                        catch (Exception) { }
                    }
                }

            }
            catch(Exception)
            {

            }

            return jasonobject;
        }

        static void SendInfotoControl(Dictionary<String, Object> jasonobject)
        {
            logIt("SendInfotoControl ++");
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var serializedResult = serializer.Serialize(jasonobject);
            Console.WriteLine(serializedResult);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:1210/modifyinfo");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(serializedResult);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    logIt("SendInfotoControl Successful.");
                }
            }
            catch (Exception ex)
            {
                logIt(ex.Message);
                logIt(ex.StackTrace);
            }
        }


        public static string BytesAsString(Int64 bytes, int onek=1000) //one kilo is 1000 or 1024
        {
            string[] suffix = { "B", "KB", "MB", "GB", "TB" };
            int i;
            double doubleBytes = 0;

            for (i = 0; (int)(bytes / onek) > 0; i++, bytes /= onek)
            {
                doubleBytes = (double)bytes / (double)onek;
            }

            return string.Format("{0:0.00} {1}", doubleBytes, suffix[i]);
        }

        public static int modifyLabelValue(String iLabel, string csPath, string csValue)
        {
            uint iRet = 100;
            try
            {
                string curl = Environment.ExpandEnvironmentVariables("%APSTHOME%curl.exe");
                logIt($"modifyLabelValue++ curl: [{curl}] label: [{iLabel}] path: [{csPath}] value: [{csValue}]");

                if (File.Exists(curl))
                {
                    string param = string.Format("http://localhost:1210/modifyinfo --data-urlencode label=\"{0}\" --data-urlencode xpath=\"{1}\" --data-urlencode value=\"{2}\"  --get", iLabel, csPath, csValue);
                    runEXE(curl, param, out iRet, 1 * 60 * 1000);
                    iRet = 0;
                }
                else
                {
                    logIt(string.Format("curl.exe is not exist[{0}]", curl));
                }
            }
            catch (System.Exception ex)
            {
                logIt(string.Format("modifyLabelValue exception: {0}", ex.ToString()));
            }

            logIt(string.Format("modifyStmodifyLabelValueatus-- return [{0}]", iRet));
            return (int)iRet;
        }


        static Boolean CheckServiceFindDevice(String sUdid, int nRetry=3)
        {
            //TODO: set idevice_id.exe full path.
            String siDevice_id=Environment.ExpandEnvironmentVariables(@"%APSTHOME%tools\winiMobileDevice\idevice_id.exe");
            if (!File.Exists(siDevice_id))
            {
                logIt($"{siDevice_id} not exist.");
                return true;
            }
            Boolean bFind = false;
            uint exitCode = 0;
            do
            {
                List<String> ret = runEXE(siDevice_id, "-l", out exitCode, 1 * 60 * 1000);
                if (ret.Any(s => s.Equals(sUdid, StringComparison.OrdinalIgnoreCase)))
                {
                    bFind = true;
                    break;
                }
                Thread.Sleep(5000);
            } while (nRetry-- > 0);
            return bFind;
        }

        static DateTime startime = DateTime.Now;

        static private void PutInfoTOXML(String iLabel, String ssNumber, String model, String version)
        {          
            Task.Factory.StartNew(() =>
            {
                modifyLabelValue(iLabel, "/labelinfo/device/iPhoneSerialNumber", ssNumber);
                modifyLabelValue(iLabel, "/labelinfo/device/deviceid", ssNumber);
                modifyLabelValue(iLabel, "/labelinfo/device/producttype", model);
                modifyLabelValue(iLabel, "/labelinfo/runtime/producttype", model);
                modifyLabelValue(iLabel, "/labelinfo/runtime/iosVersion", version);//< iosVersion addbyrequest = "True" > 12.4 </ iosVersion >
                //Read from IOS
                IniFile ini = new IniFile(Environment.ExpandEnvironmentVariables("%APSTHOME%IOSFile.ini"));
                String deviceMarketName = ini.GetString("iosmodel", model, "");
                if (!String.IsNullOrEmpty(deviceMarketName))
                {
                    modifyLabelValue(iLabel, "/labelinfo/device/model", deviceMarketName);
                    modifyLabelValue(iLabel, "/labelinfo/runtime/model", deviceMarketName);
                    modifyLabelValue(iLabel, "/labelinfo/runtime/modeltype", deviceMarketName);
                }
            });
        }

        static int Main(string[] args)
        {
            String sLabel = "";
            int retcode = -1;
            if (args.Length < 1)
            {
                logIt($"command line Error.");
                return -1;
            }

            String sFile = args[0];
            if (!File.Exists(sFile))
            {
                logIt($"{sFile} not exist.");
                return 2;
            }

            try
            {

                XDocument xml;
                using (StreamReader sr = new StreamReader(sFile, true))
                {
                    xml = XDocument.Load(sr);
                }
                //XDocument xml = XDocument.Load(sFile);
                var task = xml.Root.Elements("taskid").SingleOrDefault();
                sLabel = task.Value;
                TAG = $"[Label_{sLabel}]";

                try
                {
                    Directory.CreateDirectory(Environment.ExpandEnvironmentVariables($"%APSTHOME%FDTemporary\\label_{sLabel}"));
                }
                catch (Exception) { }

                //String sPathTetherWing = Environment.ExpandEnvironmentVariables($"%APSTHOME%..\\TetherWing\\info\\label_{sLabel}.xml");
                //if (!File.Exists(sPathTetherWing)) return 3;
                // xml = XDocument.Load(sPathTetherWing);
                String sHubName = "";
                String sHubPort = "";
                var usbinfo = xml.Root.Elements("usbhub").SingleOrDefault();
                try
                {
                    sHubName = (String)usbinfo.Attribute("name");
                    sHubPort = (String)usbinfo.Attribute("port");
                }
                catch (Exception) { }


                var devname = xml.Root.Elements("devicename").Select(x => x.Value).SingleOrDefault();
                var comport = xml.Root.Elements("comport").Select(x => x.Value).SingleOrDefault();



                Regex Reg = new Regex(@"^(i.*?\d+a\d+)aa((\d+a)+\d+)aa(.*?)$");
                Match m = Reg.Match(devname);
                if (m.Success)
                {
                    String ssNumber = m.Groups[4].Value;
                    Task dl = downloadinfo(ssNumber, sLabel);

                    //download file and extra
                    uint exitCode;
                    String sAppleSwitchControl = Environment.ExpandEnvironmentVariables(@"%APSTHOME%phonedll\PST_APE_UNIVERSAL_USB_FD\resource\AppleSwitchControl.exe");

                    String model = m.Groups[1].Value.Replace("a", ",");
                    String version = m.Groups[2].Value.Replace("a", ".");

                    //PutInfoTOXML
                    PutInfoTOXML(sLabel, ssNumber, model, version);

                    StringBuilder sb = new StringBuilder();
                    sb.Append($"-label={sLabel}  ");
                    sb.Append($"-model={model} -version={version} -supervisor  ");
                    if (!String.IsNullOrEmpty(sHubName) && !String.IsNullOrEmpty(sHubPort))
                    {
                        sb.Append($"-hubname={sHubName} -hubport={sHubPort}  ");
                    }
                    if (!String.IsNullOrEmpty(comport))
                    {
                        sb.Append($"-com={comport}  ");
                    }
                    if (PSTLoader.Properties.Settings.Default.bWaitServerdata)
                    {
                        logIt("need wait for server data.");
                        dl.Wait();
                    }
                    if (lines.ContainsKey("UniqueDeviceID"))
                    {
                        sb.Append($" -udid={lines["UniqueDeviceID"]}  ");
                        if (CheckServiceFindDevice(lines["UniqueDeviceID"]))
                        {
                            logIt("iTunes service not found the device.");
                        }
                    }
                   
                    String sWifiProfile = ExtraWifiprofile(sLabel);
                    if (File.Exists(sWifiProfile))
                    {
                        sb.Append($"-pathprofile=\"{sWifiProfile}\"  ");
                    }

                    String sArgs = sb.ToString();//$"-label={sLabel} -hubname={sHubName} -hubport={sHubPort} -model={model} -version={version} -supervisor";
                    modifyLabelValue(sLabel, "/labelinfo/runtime/message", "Robot Key");
                    List<String> ret = runEXE(sAppleSwitchControl, sArgs, out exitCode, 5 * 60 * 1000);
                    modifyLabelValue(sLabel, "/labelinfo/runtime/keyerror", exitCode.ToString());
                    if (exitCode != 0)
                    {
                        logIt($"runEXE failed errorcode=  {exitCode}.");
                        modifyLabelValue(sLabel, "/labelinfo/runtime/errorcode", "30010");
                        modifyLabelValue(sLabel, "/labelinfo/runtime/FDResult", "Fail");
                        return 10;
                    }
                    else
                    {
                        logIt("run AppleSwitchControl success");
                        //check IPA get wifi
                        Thread.Sleep(5000);
                        do
                        {
                            retcode = GetResult(ssNumber);
                            Thread.Sleep(5000);
                        } while (retcode == 134);
                       
                        if (lines.Count == 0)
                        {
                            logIt($"get info from ftp has error.now no change error code.");
                        }

                        try
                        {
                            string fn1 = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Apple", "Lockdown", $"{lines["UniqueDeviceID"]}.plist");
                            if (File.Exists(fn1))
                            {
                                File.Delete(fn1);
                            }
                        }
                        catch (Exception) { }
                        try
                        {
                            string local_dir = System.IO.Path.Combine(Environment.ExpandEnvironmentVariables($"%APSTHOME%FDTemporary\\label_{sLabel}"));
                            string local_fn = System.IO.Path.Combine(local_dir, $"{ssNumber}.zip");
                            if (File.Exists(local_fn))
                            {
                                File.Delete(local_fn);
                            }

                            System.IO.DirectoryInfo di = new DirectoryInfo(local_dir);

                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in di.GetDirectories())
                            {
                                dir.Delete(true);
                            }
                        }
                        catch (Exception)
                        {

                        }
                        if (retcode == 1) retcode += 500;
                        retcode = retcode==0?1: retcode+30000;
                        logIt($"query http response:{retcode}");
                        modifyLabelValue(sLabel, "/labelinfo/runtime/errorcode", retcode.ToString());
                        modifyLabelValue(sLabel, "/labelinfo/runtime/FDResult", retcode==1? "Success" : "Fail");
                        return retcode;
                    }
                }
                else
                {
                    modifyLabelValue(sLabel, "/labelinfo/runtime/keyerror", "11");
                    logIt($"{devname} format Error.");
                    modifyLabelValue(sLabel, "/labelinfo/runtime/errorcode", "30011");
                    modifyLabelValue(sLabel, "/labelinfo/runtime/FDResult", "Fail");
                    return 11;
                }
            }catch(Exception e)
            {
                logIt(e.ToString());
                logIt(e.StackTrace);
                modifyLabelValue(sLabel, "/labelinfo/runtime/errorcode", "30111");
                modifyLabelValue(sLabel, "/labelinfo/runtime/FDResult", "Fail");
                return 111;
            }

           
        }

        static public int GetResult(string serialnumber, string uris="http://127.0.0.1:19070/wificonn")
        {
            int ret = 118;
            try
            {
                string uri = $"{uris}?serialnumber={serialnumber}";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                //request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                //request.Headers.Add("serialnumber", serialnumber);
                String s = "";
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    s = reader.ReadToEnd();
                    logIt(s);
                }
                if (!String.IsNullOrEmpty(s))
                {
                    var serializer = new JavaScriptSerializer();
                    var result = (Dictionary<string, Object>)serializer.DeserializeObject(s);
                    if(result.ContainsKey("result"))
                    {//{"result": {"serial":"C39MLXX6FNJK","ip":"127.0.0.1:1564","errorcode":0}}
                        if (result["result"] is string)
                        {
                            if (String.Compare(result["result"].ToString(), "waiting", true) == 0)
                            {
                                ret = 134; // waiting . app need waiting
                            }
                            else
                            { ret = 124; }
                        }
                        else
                        {
                            Dictionary<string, Object> des = (Dictionary<string, Object>)result["result"];

                            if (des.ContainsKey("errorcode"))
                            {
                                ret = Convert.ToInt32(des["errorcode"]);
                                if (ret != 0) ret += 500;
                            }
                            else
                            {
                                ret = 120;
                                logIt("Result is false");
                            }
                        }
                    }
                    else
                    {
                        ret = 121;
                        logIt("Not found result key.");
                    }
                }
            }catch(Exception e)
            {
                logIt(e.ToString());
                logIt(e.StackTrace);
                ret = 119;
            }
            return ret;
        }
    }
}
