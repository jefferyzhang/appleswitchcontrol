﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WifiWebService
{
    class Program
    {
        static String TAG = "WifiWebService";
        public static void logIt(String msg)
        {
            lock (TAG)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("[{0}]:[{1}]: {2}", TAG, System.Threading.Thread.CurrentThread.Name, msg));
            }
        }

        public static void dumpVersion()
        {
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            logIt(string.Format("version={0}", myFileVersionInfo.FileVersion));
            logIt(string.Format("args={0}", System.Environment.CommandLine));
        }

        public static string[] runExe(string exeFilename, string param, out int exitCode, System.Collections.Specialized.StringDictionary env, int timeout = 60 * 1000)
        {
            List<string> ret = new List<string>();
            exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, param));
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = param;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    if (env != null && env.Count > 0)
                    {
                        foreach (DictionaryEntry de in env)
                        {
                            p.StartInfo.EnvironmentVariables.Add(de.Key as string, de.Value as string);
                        }
                    }
                    p.OutputDataReceived += (obj, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt(string.Format("[runExe]: {0}", args.Data));
                            ret.Add(args.Data);
                        }
                        if (args.Data == null)
                            ev.Set();
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        ev.WaitOne(timeout);
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                    }
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return ret.ToArray();
        }

        public static void prepareEnv(System.Collections.Specialized.StringDictionary args)
        {
            
            // netsh http add urlacl url=http://+:8080/ user=Everyone
            // netsh advfirewall firewall add rule name="FutureDial Service Port 8080" dir=in action=allow protocol=TCP localport=8080
            string s = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.System), "netsh.exe");
            if (System.IO.File.Exists(s))
            {
                int i;
                string param = string.Format("http add urlacl url=http://+:{0}/ user=Everyone", WifiWebService.Properties.Settings.Default.port);
                string[] lines = runExe(s, param, out i, null);
                param = string.Format("advfirewall firewall add rule name=\"FutureDial Service Port {0}\" dir=in action=allow protocol=TCP localport={0}", WifiWebService.Properties.Settings.Default.port);
                lines = runExe(s, param, out i, null);
            }
        }

        static void start(System.Threading.EventWaitHandle quit, System.Collections.Specialized.StringDictionary args)
        {
            prepareEnv(args);
            Thread threadWebService = new Thread(() =>
            {
                WebService.run(quit, args);
            });
            threadWebService.IsBackground = true;
            threadWebService.Name = "WebService";
            threadWebService.Start();
            System.Console.WriteLine("press any key to terminate.");
            while (!quit.WaitOne(1000))
            {
                if (System.Console.KeyAvailable)
                {
                    quit.Set();
                }
            }

            threadWebService.Join();
            quit.Close();
        }

        static void Main(string[] args)
        {
            System.Configuration.Install.InstallContext _args = new System.Configuration.Install.InstallContext(null, args);
            System.Threading.Thread.CurrentThread.Name = "MainThread";
            if (_args.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for debug, press any key to continue...");
                System.Console.ReadKey();
            }
            dumpVersion();
            if (_args.IsParameterTrue("start-service"))
            {
                bool own;
                System.Threading.EventWaitHandle _quit = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset, WifiWebService.Properties.Settings.Default.eventname, out own);
                if (!own)
                {
                    // another instance already running.
                    logIt("another instance already running. exiting...");
                    _quit.Close();
                }
                else
                {
                    start(_quit, _args.Parameters);
                }
            }
            else if (_args.IsParameterTrue("kill-service"))
            {
                try
                {
                    System.Threading.EventWaitHandle _quit = System.Threading.EventWaitHandle.OpenExisting(WifiWebService.Properties.Settings.Default.eventname);
                    _quit.Set();
                }
                catch (Exception) { }
            }
        }
    }
}
