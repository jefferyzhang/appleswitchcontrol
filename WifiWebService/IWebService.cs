﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WifiWebService
{
    [ServiceContract]
    interface IWebService
    {
        [OperationContract]
        [WebGet(UriTemplate = "wifi", ResponseFormat = WebMessageFormat.Json)]
        Stream getWifiInfo();
    }
    class WebService : IWebService
    {
        

        public Stream getWifiInfo()
        {
            Stream ret = null;
            String sPath = Environment.ExpandEnvironmentVariables("%APSTHOME%wifi.json");
            if (File.Exists(sPath))
            {
                try
                {
                    ret = new MemoryStream(File.ReadAllBytes(sPath));
                }
                catch (Exception) { }
            }
            return ret;
        }

        #region main run
        public static void run(System.Threading.EventWaitHandle quitEvent, System.Collections.Specialized.StringDictionary args)
        {
            try
            {
                Uri baseAddress = new Uri(string.Format("http://0.0.0.0:{0}/", WifiWebService.Properties.Settings.Default.port));
                WebServiceHost svcHost = new WebServiceHost(typeof(WebService), baseAddress);
                //WebHttpBinding b = new WebHttpBinding();
                //b.Name = "twControllerService";
                //b.HostNameComparisonMode = HostNameComparisonMode.Exact;
                //svcHost.AddServiceEndpoint(typeof(ItwControllerService), b, "");
                svcHost.Open();
                Program.logIt("WebService::run: WebService is running");
                quitEvent.WaitOne();
                Program.logIt("WebService::run: Service is going to terminated.");
                svcHost.Close();
                DateTime _start = DateTime.Now;
                while (svcHost.State != CommunicationState.Closed)
                {
                    System.Threading.Thread.Sleep(1000);
                    if ((DateTime.Now - _start).TotalSeconds > 30)
                        break;
                }
                Program.logIt("WebService::run: Service is terminated.");

            }
            catch (Exception ex)
            {
                Program.logIt(string.Format("WebService::run: {0}", ex.Message));
                Program.logIt(string.Format("WebService::run: {0}", ex.StackTrace));
            }
        }
        #endregion
    }
}