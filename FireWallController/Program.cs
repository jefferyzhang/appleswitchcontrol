﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FireWallController
{
    class Program
    {
        public static void logIt(String msg)
        {
            System.Diagnostics.Trace.WriteLine(msg);
        }

        static public List<string> runEXE(string exeFilename, string args, out uint exitCode, int timeout = 100 * 1000)
        {
            List<string> ret = new List<string>();
            exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, args));
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = args;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;

                    p.OutputDataReceived += (s, o) =>
                    {
                        if (o.Data == null)
                            ev.Set();
                        else
                        {
                            logIt(string.Format("[runExe]: {0}", o.Data));
                            ret.Add(o.Data);
                        }
                    };
                    p.Start();
                    p.BeginOutputReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        ev.WaitOne(timeout);
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = (uint)p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                        logIt(string.Format("[runExe]: Kill by caller due to timeout={0}", timeout));
                    }
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return ret;
        }

        static void Main(string[] args)
        {
            // netsh http add urlacl url=http://+:8080/ user=Everyone
            // netsh advfirewall firewall add rule name="FutureDial Service Port 8080" dir=in action=allow protocol=TCP localport=8080
            string s = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.System), "netsh.exe");
            if (System.IO.File.Exists(s))
            {
                uint i;

                var serializer = new JavaScriptSerializer();
                var jsondelay = (Dictionary<string, int[]>)serializer.DeserializeObject(FireWallController.Properties.Settings.Default.TrustPorts);

                if (jsondelay.ContainsKey("ports"))
                {
                    int[] ports = jsondelay["ports"];
                    foreach(var p in ports)
                    {
                        string param = string.Format("http add urlacl url=http://+:{0}/ user=Everyone", p);
                        var lines = runEXE(s, param, out i);
                        param = string.Format("advfirewall firewall add rule name=\"FutureDial Service Port {0}\" dir=in action=allow protocol=TCP localport={0}", p);
                        lines = runEXE(s, param, out i);
                    }
                }
            }
        }
    }
}
